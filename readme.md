### MiniWatson Quesion Answering (QA) system:

These type of systems covers vast type of questions and even more number of question types. Here we are covered with some question types. Along with this there exists another file ```SQLCOMMANDS_NLP.txt``` which shows only the sample set of the these question types. If you need to add new questions, you just need to maintain the rules of these question types. For additional information on the project you could refer the problem statement in MiniWatson_project.pdf.
Please let me know if you want to compile and execute the source, I could share you libs and models.

#### Packages 
There are 6 packages in the project namely, 

* main: Driver package
* qawparser: Parser package
* category: Category identification package
* common: General data shared across classes
* semantics: Building semantics and generation of sql query
* tts: Text-to-speech engine package
* ui: Graphical user interface

#### Major Classes:
* main.MiniWatsonApp.java: The driver class implements the interface IWatson. There is a sample test code present in the main class.
* qawparser.AbstractParser.java: The abstract class implementation of a parser generation.
* qawparser.OpenNLPWrapParser.java: This is the concrete class of implementation which uses ```OpenNLP``` libraries for ```parsing, tokenization, POStagging``` etc.
* qawparser.StanfordWrapParser.java: This is the concrete class of implementation which uses ```Stanford Parser``` libraries for ```parsing, tokenization, POStagging``` etc.
* category.CategoryIdentification.java: This class has the logic of identifying the category of the questions.
* category.WordNetUtils.java: This class exposes the implementation of ```WordNet``` into the application. It helps to identify ```synonyms, hypernyms, hyponyms, pertainyms``` etc.
* category.CommonData.java: This is a helper class contains the generic data which are shared across classes.
* semantics.SemanticAnalyzer.java: Constructs the custom data structure of syntactic tree called ```'SemAttachmentTree'```. It initiates the _semantics_ and _sql generation._
* semantics.SemAttachmentTree.java: The core class of building semantics and generating sql expressions for the given query.
* semantics.SQLQueryGenerator.java: This class bind the interface with sqlite library for firing the generated query and retrieving the result set.