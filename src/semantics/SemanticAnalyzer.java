package semantics;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import opennlp.tools.parser.Parse;
import qawparser.OpenNLPWrapParser;
import ui.ParseTreeUI;
import common.CommonData;
import common.CommonData.CONSTITUENTTYPE;
import common.CommonData.QUESTIONTYPE;

public class SemanticAnalyzer {

	QUESTIONTYPE m_questionType;
	OpenNLPWrapParser m_nlpParser;
	//Stack<String> m_ptConstituents;
	//Stack<String> m_ptLabel;
	SemAttachmentTree m_SemAttachTree;
	String m_sqlExpression;
	Stack<SemAttachmentTree> m_stackTree;
	Logger log =  Logger.getLogger(getClass());
	
	private static SemanticAnalyzer m_semAnalyzer = null;
	
	//singelton
	public static SemanticAnalyzer getInstance(){
		
		if(m_semAnalyzer == null)
			m_semAnalyzer = new SemanticAnalyzer();
		
		return m_semAnalyzer;
	}
	
	public static void deleteInstance(){
		if(m_semAnalyzer != null)
			m_semAnalyzer = null;
	}
	
	private SemanticAnalyzer() {
		m_nlpParser = OpenNLPWrapParser.getInstance();
		//m_ptConstituents = new Stack<String>();
		//m_ptLabel = new Stack<String>();
		m_stackTree = new Stack<SemAttachmentTree>();
		m_sqlExpression = new String();
		BasicConfigurator.configure();
		log.setLevel(Level.OFF);
	}

	public String getGeneratedsqlExpressionForQuery() {
		//remove unwanted character
		String sqlquery = m_SemAttachTree.getM_sqlExpression().replace(",", "").trim();
		return sqlquery;
	}
	
	public SemAttachmentTree getM_SemAttachTree() {
		return m_SemAttachTree;
	}
	
	public void analyzeConstituents(String sentence){
		//get the parse tree of the sentence
		Parse[] pt = m_nlpParser.parseSentence1(sentence);
		
		/*//some pre-conditions for the validity of the parse trees
		int i = 0;
		for(i = 0; i< pt.length; i++){
			String treestr = convertPTtoString(pt[i]);
			
			if(treestr.contains("than")){
				if(treestr.contains("JJR"))
					break;
				else
					continue;
			}
			//System.out.println(convertPTtoString(pt[i]));
		}*/
		
		//System.out.println(convertPTtoString(pt[0]));
		
		//create a sem attachment tree
		getListofChildren(pt[0]);
		
		/*log.debug("Tree Constituents:");
		ListIterator<String> itrConst = m_ptConstituents.listIterator();
		while(itrConst.hasNext()){
			System.out.println(m_ptConstituents.pop());
		}*/
		
		//print sem tree
		/*Queue<SemAttachmentTree> semqueue = new LinkedList<SemAttachmentTree>();
		semqueue.add(m_SemAttachTree);
		
		while(!semqueue.isEmpty()){
			
			//parent node
			SemAttachmentTree tree = semqueue.remove();
			String root = tree.getM_nodeValue();
			System.out.println(root);
			//children value
			for(SemAttachmentTree child: tree.getchildNodesList()){
				System.out.println(child.getM_nodeValue());
			}
			
			System.out.println("----------------------");
				
			//add children in the queue
			semqueue.addAll(tree.getchildNodesList());	
		}*/
		
		//second pass to populate the sql exressions
		buildSQLExpressions();
		
		System.out.println(m_SemAttachTree.getM_sqlExpression());
		
		/*log.debug("Labels:");
		ListIterator<String> itrLbl = m_ptLabel.listIterator();
		while(itrLbl.hasNext()){
			System.out.println(m_ptLabel.pop());
		}
		
		ListIterator<String> itrLbl = m_ptLabel.listIterator();
		while(itrLbl.hasNext()){
			System.out.println(itrLbl.next());
		}*/
	}
	
	//get the semantic attachment of the constituents
	//categorize the question
	//form the sql query and get the answers

	private void getListofChildren(Parse root){	
		
		//semantic root
		if( null == m_SemAttachTree)
			m_SemAttachTree = new SemAttachmentTree();
		
		Queue<Parse> parseTree = new LinkedList<Parse>();
		Queue<SemAttachmentTree> semAttachTreequeue = new LinkedList<SemAttachmentTree>();
		
		//get the first parse generated
		parseTree.add(root);
		semAttachTreequeue.add(m_SemAttachTree);
		
		while(!parseTree.isEmpty() && !semAttachTreequeue.isEmpty()){
			
			Parse node = parseTree.remove();
			String rootValue = convertPTtoString(node);
			SemAttachmentTree semroot = semAttachTreequeue.remove();
			
			//add to the stack for sql population
			m_stackTree.push(semroot);
			
			//m_ptConstituents.push(rootValue);
			//push all the types of the constituents
			String tokenType = node.getType();
			//m_ptLabel.push(tokenType);
			
			if(node.isPosTag()){
				semroot.setM_eConstType(CONSTITUENTTYPE.POS);
				//System.out.println("It is POS");
			}
			
			
			//System.out.println("TokenType: " + tokenType);
			//System.out.println("RootValue: " + rootValue);
			if( tokenType.equalsIgnoreCase("TK")){
				semroot.setM_nodeValue(rootValue);
				semroot.setM_eConstType(CONSTITUENTTYPE.POS);
			}
			else{
				semroot.setM_nodeValue(tokenType);
				semroot.setM_eConstType(CONSTITUENTTYPE.CHUNK);
			}
			
			//add all the parse node children
			Parse[] ptchild = node.getChildren();
			
			//build sem att tree
			List<SemAttachmentTree> childList = convertPTtoSemAttTree(semroot, ptchild);
					
			//creating the tree
			semroot.setchildNodesList(childList);
			
			//attaching the semantics
			semroot.attachSemantics(); 
			
			//add the semchild to the list
			semAttachTreequeue.addAll(childList);
			
			//push children to the queue
			parseTree.addAll(Arrays.asList(ptchild));	
		}		
	}
	
	private void buildSQLExpressions(){	
			
		//traverse stack
		while(!m_stackTree.isEmpty()){
			SemAttachmentTree subTree = m_stackTree.pop();
			
			//set second pass
			subTree.setFirstpass(false);
			
			subTree.attachSemantics();
		}	
	}
	
	private String convertPTtoString(Parse p){
		StringBuffer buffer = new StringBuffer();
		p.show(buffer);
		return buffer.toString();
	}
	
	private List<SemAttachmentTree> convertPTtoSemAttTree(SemAttachmentTree par, Parse[] children){
		
		List<SemAttachmentTree> list = new LinkedList<SemAttachmentTree>();
		
		//create and add to the list
		for(int i = 0; i < children.length; i++){
			
			String tokenType = children[i].getType();
			String rootValue = convertPTtoString(children[i]);
			SemAttachmentTree tree = new SemAttachmentTree();
			//System.out.println("TokenType: " + tokenType);
			//System.out.println("RootValue: " + rootValue);
			
			//add tokens accordingly
			if( tokenType.equalsIgnoreCase("TK"))
				tree.setM_nodeValue(rootValue);
			else
				tree.setM_nodeValue(tokenType);			
			
			tree.setParent(par);
			list.add(tree);
		}
		
		return list;
	}
	
	public static void main(String[] args){
		SemanticAnalyzer sa = new SemanticAnalyzer();
		String sent = "Which is the lowest mountain in the world?";
		//sent = "Who won the oscar for best actor in 2005?";
		//sent = "What is the capital of Bahrain?";
		//sent = "Is Rome the capital of Italy?";
		//sent = "Is the Pacific shallower than the Atlantic?";
		//sent = "What is the capital of South Africa?";
		//sent = "Who directed Hugo?";
		//sent = "Did Razzoli win the men slalom in 2010?";
		//sent = "Who won the oscar for best supporting actress in 2005?";
		sent = "Did Swank win the oscar in 2005?";
		sa.analyzeConstituents(sent);
		SQLQueryGenerator sql = new SQLQueryGenerator();
		try {
			sql.executeSQLExpression(CommonData.getDBType(), sa.getGeneratedsqlExpressionForQuery());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

