package semantics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import common.CommonData;
import common.CommonData.SQLTABLES;

public class SQLQueryGenerator {

	private Logger log = Logger.getLogger(getClass());
	List<String> m_sQueryResponseList;
	private Connection m_Conn;

	public SQLQueryGenerator() {
		BasicConfigurator.configure();
		log.setLevel(Level.DEBUG);
		m_sQueryResponseList = new ArrayList<String>();
	}

	public void executeSQLExpression(SQLTABLES edbname, String query) throws SQLException, ClassNotFoundException {
		ResultSet rs = executeQuery(edbname, query);
		while (rs.next()) {
			m_sQueryResponseList.add(rs.getString(1));
		}
		System.out.println("Answer = " + m_sQueryResponseList);
		rs.close();
		m_Conn.close();
	}
	
	public List<String> getM_sQueryResponse() {
		return m_sQueryResponseList;
	}
	
	private ResultSet executeQuery(SQLTABLES edbname, String query) throws SQLException, ClassNotFoundException{
		
		String dbpath =  "db/project_db/";
		switch(edbname){
		case OLYMPICS:
			dbpath = dbpath.concat("Winter-Olympics-2006-2010.sqlite"); 
			break;
		case OSCAR:
			dbpath = dbpath.concat("imdb-oscar-movie.sqlite");
			break;
		case GEOGRAPHY:
			dbpath = dbpath.concat("WorldGeography.sqlite");
			break;
		default:
			log.debug("Invalid db.");
			return null;
		}

		Class.forName("org.sqlite.JDBC");
		String connstring = "jdbc:sqlite:" + dbpath;

		m_Conn = DriverManager.getConnection(connstring);
		Statement stat = m_Conn.createStatement();
		stat.setQueryTimeout(30);
		
		//remove unwanted character
		query = query.replace(",", "").trim();
		
		return stat.executeQuery(query);
	}
	
	/* time consuming job*/
	public String verifyandGetTableColumnName(SQLTABLES edbname, String tablevalue ) throws SQLException, ClassNotFoundException{
	
		List<String> tableNames = CommonData.getDBTableRelationMap().get(edbname);
		
		for(String tablename: tableNames){
		
			List<String> attributeNames = CommonData.getTableAttrRelationMap().get(tablename);
			String query = "select * from " + tablename;  
			ResultSet rs = executeQuery(edbname, query);
			
			//check whether this value exist
			while (rs.next()) {
				for(String attr: attributeNames){
					if(rs.getString(attr).contains(tablevalue)){
						rs.close();
						m_Conn.close(); 
						return attr;
					}
				}
			}
			rs.close();
			m_Conn.close();
		}
		
		return null;
	}
	
	
	public static void main(String[] args){
		SemanticAnalyzer sa = SemanticAnalyzer.getInstance();
		String sent = "which is the highest mountain in the world?";
		sa.analyzeConstituents(sent);
		
		//query to the table
		SQLQueryGenerator sqlgen = new SQLQueryGenerator();
		try {
			
			sqlgen.executeSQLExpression(CommonData.getDBType(), sa.getGeneratedsqlExpressionForQuery());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
