package semantics;

import common.CommonData.SQLCOMMANDTYPE;

public class SQLCommands {
	
	private SQLCOMMANDTYPE m_eSqlCmdType;
	private String m_SqlExpression;
	
	public SQLCOMMANDTYPE getM_eSqlCmdType() {
		return m_eSqlCmdType;
	}
	public void setM_eSqlCmdType(SQLCOMMANDTYPE m_eSqlCmdType) {
		this.m_eSqlCmdType = m_eSqlCmdType;
	}
	public String getM_SqlExpression() {
		return m_SqlExpression;
	}
	public void setM_SqlExpression(String m_SqlExpression) {
		this.m_SqlExpression = m_SqlExpression;
	}	
}
