package semantics;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import qawparser.OpenNLPWrapParser;

import common.CommonData;
import common.CommonData.CONSTITUENTTYPE;
import common.CommonData.PREPTYPE;
import common.CommonData.QUESTIONTYPE;
import edu.smu.tspell.wordnet.SynsetType;

public class SemAttachmentTree{

	private SemAttachmentTree  m_parent;
	private List<SemAttachmentTree>  m_childNodesList;
	private String m_attachment;
	private String m_sqlExpression;
	//private SQLCommands m_SqlCommand;
	private String m_nodeValue;
	boolean m_bFirstPass;
	private CONSTITUENTTYPE m_eConstType;
	OpenNLPWrapParser parser = OpenNLPWrapParser.getInstance();
	private Logger m_log = Logger.getLogger(getClass());

	public SemAttachmentTree(){
		m_parent = null;
		m_childNodesList = new ArrayList<SemAttachmentTree>();
		m_attachment = new String();
		m_sqlExpression = new String();
		//m_SqlCommand = new SQLCommands();
		m_nodeValue = new String();
		m_bFirstPass = true;
		m_eConstType = CONSTITUENTTYPE.CHUNK;

		BasicConfigurator.configure();
		m_log.setLevel(Level.OFF);
	}

	public void setParent(SemAttachmentTree par){
		m_parent = par;
	}

	public SemAttachmentTree getparent() {
		return m_parent;
	}

	public void setchildNodesList(List<SemAttachmentTree> childList){
		m_childNodesList = childList;
	}

	public List<SemAttachmentTree> getchildNodesList() {
		return m_childNodesList;
	}

	public void setFirstpass(boolean bStatus){
		m_bFirstPass = bStatus;
	}

	public String getM_attachment() {
		return m_attachment;
	}

	public void setM_attachment(String m_attachment) {
		this.m_attachment = m_attachment;
	}

	public String getM_nodeValue() {
		return m_nodeValue;
	}

	public void setM_nodeValue(String m_nodeValue) {
		this.m_nodeValue = m_nodeValue;
	}

	public String getM_sqlExpression() {
		return m_sqlExpression;
	}

	public void setM_sqlExpression(String m_sqlExpression) {
		this.m_sqlExpression = m_sqlExpression;
	}

	public void setM_eConstType(CONSTITUENTTYPE m_eConstType) {
		this.m_eConstType = m_eConstType;
	}

	public CONSTITUENTTYPE getM_eConstType() {
		return m_eConstType;
	}

	/**
	 * First pass: Attaches semantics from root to the leaf
	 * Second pass: populate sql expression from leaf to the root
	 */
	public void attachSemantics(){

		if( null == m_childNodesList){
			m_log.debug("Child list nodes are null"); 

			//attach its values as constants. this state we will be in leaf level
			m_attachment = getM_nodeValue();
			return;
		}

		int count = m_childNodesList.size();
		// S or VP or NP or PP or VP NP or VP PP or NP VP or NP PP 

		if( 0 == count){ //for the leaf nodes
			m_attachment = ""; //do not attach any attachments for POS nodes in the leaf level

			m_sqlExpression = m_nodeValue;
			//m_SqlCommand.setM_eSqlCmdType(SQLCOMMANDTYPE.TEXT);
			//m_SqlCommand.setM_SqlExpression(m_nodeValue);
		}
		else if( 1 == count){ // S or VP or NP or PP

			String token = m_childNodesList.get(0).getM_nodeValue();

			if(m_bFirstPass){
				//neglect these things in assignment
				if(token.equalsIgnoreCase("TOP") || token.equalsIgnoreCase(".") || token.equalsIgnoreCase("?"))
				{
					m_attachment = "";
					m_sqlExpression = "";
					return;
				}

				/*if(m_childNodesList.get(0).getM_eConstType().equals(CONSTITUENTTYPE.CHUNK)){
				m_attachment = token + ".sem";
			}*/
				//first pass: attach semantics
				if(token.equalsIgnoreCase("S") || token.equalsIgnoreCase("SBAR") ||
						token.equalsIgnoreCase("NP") ||
						token.equalsIgnoreCase("VP") ||
						token.equalsIgnoreCase("PP") ||
						token.equalsIgnoreCase("WDT") ||
						token.equalsIgnoreCase("VBZ") ||
						token.equalsIgnoreCase("NN") ||
						token.equalsIgnoreCase("NNP") ||
						token.equalsIgnoreCase("CD")){
					m_attachment = token + ".sem";
					m_sqlExpression = "";
					//return;
				}
				else{
					//check for POS
					String[] tokenA = new String[1];
					tokenA[0] = token;
					String[] postags = parser.POStagSentence(tokenA);
					if(postags.length > 0){
						m_attachment = token;
					}
				}
			}
			else{
				//check whether NP is a number or a table attribute
				if( m_nodeValue.equalsIgnoreCase("DT") || 
						m_nodeValue.equalsIgnoreCase("VBZ") ||
						m_nodeValue.isEmpty() ){
					m_sqlExpression = "";
					return;
				}

				//set the prep type
				if(m_nodeValue.equalsIgnoreCase("IN") ){
					if( m_childNodesList.get(0).getM_sqlExpression().equalsIgnoreCase("in"))
						CommonData.setM_prepYesNoType(PREPTYPE.IN);
					else if( m_childNodesList.get(0).getM_sqlExpression().equalsIgnoreCase("of"))
						CommonData.setM_prepYesNoType(PREPTYPE.OF);
					else if( m_childNodesList.get(0).getM_sqlExpression().equalsIgnoreCase("than"))
						CommonData.setM_prepYesNoType(PREPTYPE.THAN);

					m_sqlExpression = "";
					return;
				}

				/*else if( m_nodeValue.equalsIgnoreCase("VP") ||
					m_nodeValue.equalsIgnoreCase("S") )*/
				else
				{
					String value = m_childNodesList.get(0).getM_sqlExpression();
					String attr;
					//check whether NP is a number or a table attribute
					String tablename;

					//load db details
					CommonData.getDBTableRelationMap();
					CommonData.getTableAttrRelationMap();

					if(CommonData.getM_qtype() == QUESTIONTYPE.YESNO){

						if(token.equalsIgnoreCase(".") || token.equalsIgnoreCase("?")){
							m_sqlExpression = "";
							return;
						}

						//for propernouns just propagate to the parent
						if(token.equalsIgnoreCase("NNP")){
							m_sqlExpression += value;
							return;
						}

						if(CommonData.getM_prepYesNoType() == PREPTYPE.THAN){
							if(m_nodeValue.equalsIgnoreCase("JJR")){ //comparison is always JJR	
								if( (attr = CommonData.isaTableAttr(value, SynsetType.ADJECTIVE)) != null){
									if( (tablename = CommonData.isaTable(attr)) != null){ //check NP in table
										m_sqlExpression = CommonData.formYESNOMainSQLStmt(tablename, tablename, value);
										return;
									}
								}	
							}
							m_sqlExpression += value;
							return;
						}

						if( (tablename = CommonData.isaTable(value)) != null){ //check NN in table
							m_sqlExpression = CommonData.formYESNOMainSQLStmt(tablename, value);
						}
						else
							m_sqlExpression += value;

						return;		
					}	

					else if(CommonData.getM_qtype() == QUESTIONTYPE.WHQ){

						if(token.equalsIgnoreCase("directed")){ 
							m_sqlExpression = CommonData.formWHMainSQLStmt("person", "direct", "");
							return;
						}

						if( (tablename = CommonData.isaTable(value)) != null){ //check NP in table

							//capital
							if(tablename.contains("capital"))
								m_sqlExpression = CommonData.formWHMainSQLStmt(tablename, tablename, value);
							else
								m_sqlExpression = m_childNodesList.get(0).getM_sqlExpression() + " ";


							return;
						}

						//else statement
						m_sqlExpression += m_childNodesList.get(0).getM_sqlExpression() + " ";
					}
				}

				/*else if( m_nodeValue.equalsIgnoreCase("SBAR") || 
						m_nodeValue.equalsIgnoreCase("TOP") ){
					m_sqlExpression += m_childNodesList.get(0).getM_sqlExpression() + " ";
				}*/
			}
		}
		else if( 2 == count){ //VP NP or VP PP or NP VP or NP PP or IN PP or VBZ NP or JJ NP

			String token1 = m_childNodesList.get(0).getM_nodeValue();
			String token2 = m_childNodesList.get(1).getM_nodeValue();

			if(m_bFirstPass){			
				if(token1.equalsIgnoreCase("DT"))
					m_attachment = token2 + ".sem";
				else
					m_attachment = token1 + ".sem" + "," + token2 + ".sem";
			}
			else{
				//PREP: condition phrase
				if(token1.contains("IN") && token2.contains("NP")){
					if(CommonData.getM_qtype() == QUESTIONTYPE.YESNO){

						if(CommonData.getM_prepYesNoType() == PREPTYPE.IN){
							if(m_childNodesList.get(1).getM_attachment().equalsIgnoreCase("NNP.sem")){ //Is France in Europe?
								String tablename = "COUNTRYCONTINENTS";
								String name = m_childNodesList.get(1).getM_sqlExpression();
								
								if(CommonData.isContinent(name))
									m_sqlExpression = lamdaReduction(CommonData.formYESNOMainSQLStmt(tablename, "countrycontinents"), 
											name );
								else //movie name
									m_sqlExpression = name;
							}
							else if(m_childNodesList.get(1).getM_attachment().contains("CD") ||
									m_childNodesList.get(1).getM_attachment().contains("NN") ){ //... in 2010?
								m_sqlExpression = m_childNodesList.get(1).getM_sqlExpression();
							}
						}
						else						
							m_sqlExpression = m_childNodesList.get(1).getM_sqlExpression();
					}
					else{ //IN NP: WHQ						

						//load db details
						CommonData.getDBTableRelationMap();
						CommonData.getTableAttrRelationMap();

						//check whether NP is a number or a table attribute
						String attr;
						String value2 = m_childNodesList.get(1).getM_sqlExpression();

						if(value2.isEmpty())
							m_sqlExpression = "";
						else if( CommonData.isaYear(value2.trim()))
							m_sqlExpression = value2;
						else if( (attr = CommonData.isaTableAttr(value2, SynsetType.NOUN)) != null)
							m_sqlExpression = "";
						else if(value2.contains(","))
							m_sqlExpression = value2;
						else
							if(m_childNodesList.get(1).getchildNodesList().get(0).getM_nodeValue().contains("NNP") ||
									m_childNodesList.get(1).getM_attachment().contains("JJS.sem,NN.sem") ||
									m_childNodesList.get(1).getM_sqlExpression().contains("BEST"))
								m_sqlExpression = value2;	
					}
				}	

				//VP NP, DT NN, NP PP
				else if(token1.contains("VP") && token2.contains("NP") ||
						token1.contains("DT") && token2.contains("NN") ||
						token1.contains("NP") && token2.contains("PP") ||
						token1.contains("JJS") && token2.contains("NN") ){ 

					if(CommonData.getM_qtype() == QUESTIONTYPE.YESNO){

						if(token1.contains("DT")){
							m_sqlExpression = m_childNodesList.get(1).getM_sqlExpression() + " ";
						}
						else if(CommonData.getM_prepYesNoType() == PREPTYPE.THAN){
							m_sqlExpression += lamdaReduction( m_childNodesList.get(0).getM_sqlExpression(), 
									m_childNodesList.get(1).getM_sqlExpression() );
						}
						else
							m_sqlExpression = lamdaReduction( m_childNodesList.get(0).getM_sqlExpression(), 
									m_childNodesList.get(1).getM_sqlExpression() );
					}
					else if(CommonData.getM_qtype() == QUESTIONTYPE.WHQ){

						if(token1.contains("DT")){
							m_sqlExpression = m_childNodesList.get(1).getM_sqlExpression() + " ";
						}
						else
						{
							String value1 = m_childNodesList.get(0).getM_sqlExpression().trim();
							String value2 = m_childNodesList.get(1).getM_sqlExpression().trim();

							if(value1.contains("oscar"))
								m_sqlExpression = value2.trim();
							else if(CommonData.isaYear(value2)){
								m_sqlExpression = value1 + "," + value2;
							}
							else if(value1.contains("best")){	
								m_sqlExpression = lamdaReduction(value1, value2);

								if(value1.equalsIgnoreCase("best")){
									if(m_sqlExpression.contains("actor"))
										m_sqlExpression = "BEST-ACTOR";	
									else if(m_sqlExpression.contains("actress"))
										m_sqlExpression = "BEST-ACTRESS";
									else if(m_sqlExpression.contains("director"))
										m_sqlExpression = "BEST-DIRECTOR";
								}
							}
							else if(value1.contains("capital") && CommonData.isCountry(value2)){
								m_sqlExpression = lamdaReduction(value1, value2);
							}
							else
								m_sqlExpression = m_childNodesList.get(0).getM_sqlExpression() + "," +
								m_childNodesList.get(1).getM_sqlExpression();
								
						}
					}
				}
				else if(token1.contains("VBZ") && token2.contains("NP")){
					//check whether NP is a number or a table attribute
					m_sqlExpression += m_childNodesList.get(1).getM_sqlExpression();
				}
				else if(token1.contains("NNP") && token2.contains("NNP") ||
						token1.contains("VBD") && token2.matches("NP|S") ||
						token1.contains("NNP") && token2.contains("CD")){

					//best actress
					if(m_childNodesList.get(0).getM_attachment().contains("won")){

						String[] cond = m_childNodesList.get(1).getM_sqlExpression().split(","); 
						
						//movie
						if(cond.length == 2){
						String attrType =  cond[0].trim();
						String year = cond[1].trim();

						//only for movie
						if(attrType.contains("BEST"))
							m_sqlExpression = CommonData.formWHMainSQLStmt("oscar", "won", attrType, year);
						}
						
						//sport: woman+category+[location]+[year]
						if(cond.length == 3){
							String gender =  cond[0].trim();
							String sport = cond[1].trim();
							String locationOrYear = cond[2].trim();

							if(!CommonData.isaYear(locationOrYear)) //no need to send vancouver(implicit)
								locationOrYear = "";
								
							if(gender.matches("m[ae]n"))
								gender = "M";
							else if(gender.matches("[w][o]m[ae]n"))
								gender = "F";
							
							//only for sport
							if(gender.matches("[MF]")) //men or man or woman or women
								m_sqlExpression = CommonData.formWHMainSQLStmt("athletes", "won", "vancouver", gender, sport, locationOrYear);
							}
					}
					else						
						m_sqlExpression = lamdaReduction(m_childNodesList.get(0).getM_sqlExpression(), m_childNodesList.get(1).getM_sqlExpression());
				}
				else if( token1.contains("NP") && token2.contains("VP") || //woman+figureskating+country
						 token1.matches("NNS?") && token2.matches("NN|FW")){ 
					m_sqlExpression = m_childNodesList.get(0).getM_sqlExpression() + "," +
					m_childNodesList.get(1).getM_sqlExpression();
				}
				else if(token1.matches("VBG?") && token2.contains("PP") ||
						token1.matches("JJ") && token2.contains("NN") ){ 
					String value1 = m_childNodesList.get(0).getM_sqlExpression();
					String value2 = m_childNodesList.get(1).getM_sqlExpression();
					
					//Did Neeson star ...
					if(value1.contains("star")){
						m_sqlExpression = CommonData.formYESNOMainSQLStmt("oscar", "star");
						m_sqlExpression = lamdaReduction(m_sqlExpression, value2);
					}
					else
						m_sqlExpression = value1 + "," + value2;
				}
				else if(token1.matches("NNP") && token2.contains("POS")){ //possesive proper nouns
					m_sqlExpression = m_childNodesList.get(0).getM_sqlExpression() + 
					m_childNodesList.get(1).getM_sqlExpression().replace("'", "%");
				}
				else if(token1.matches("NP") && token2.contains("NN")){
					m_sqlExpression = m_childNodesList.get(0).getM_sqlExpression() + " " +
					m_childNodesList.get(1).getM_sqlExpression();
				}
			} 			
		}
		else if( 3 == count || 4 == count){
			String token1 = m_childNodesList.get(0).getM_nodeValue();
			String token2 = m_childNodesList.get(1).getM_nodeValue();
			String token3 = m_childNodesList.get(2).getM_nodeValue();

			String token4 = null;
			if( 4 == count )
				token4 = m_childNodesList.get(3).getM_nodeValue();

			if(m_bFirstPass){
				if( m_parent.getM_nodeValue().equalsIgnoreCase("TOP") ){
					// the first token for the 'S' node
					//there are two types of configuration
					//1. yestype
					//2. WhQ type	
					QUESTIONTYPE qtype = identifyQuestionType(token1);
					CommonData.setM_qtype(qtype);

					//generate sql expression
					switch(qtype){
					case WHQ:
						m_sqlExpression = "SELECT "; // need to attach NAME accordingly
						m_attachment = token2 + ".sem"; //no need to consider '?' here 
						break;
					case YESNO:
						m_sqlExpression = "SELECT COUNT(*) ";	
						if( 3 == count )
							m_attachment = token1 + ".sem" + "," + token2 + ".sem"; //no need to consider '?' here 
						else if( 4 == count )
							m_attachment = token2 + ".sem" + "," + token3 + ".sem"; //no need to consider '?' here 
						break;
					default:
					}	
				}

				// the korean woman - DT JJ NN
				// the highest mountain - DT JJS NN
				// the pacific deeper DT NNP JJR
				else if( token1.contains("DT") && token2.contains("JJS") && token3.contains("NN") ||
						token1.contains("DT") && token2.contains("JJ") && token3.contains("NN") ||
						token1.contains("DT") && token2.contains("NNP") && token3.contains("JJR") ||
						token1.contains("DT") && token2.contains("NNS") && token3.contains("NN")){
					//SEM ATTACHMENTS
					m_attachment = token1 + ".sem," + token2 + ".sem," + token3 + ".sem";
				}
			}
			else
			{
				if(CommonData.getM_qtype() == QUESTIONTYPE.YESNO){

					String value1 = m_childNodesList.get(0).getM_sqlExpression().trim();
					String value2 = m_childNodesList.get(1).getM_sqlExpression().trim();
					String value3 = m_childNodesList.get(2).getM_sqlExpression().trim();

					if(token1.contains("VB") && token2.contains("NP") && token3.contains("PP")){

						if(value1.contains("won") || value1.contains("win")){

							if(value2.contains("men")){ //win sportcategory year
								String gender = value2.split(",")[0].trim();
								String sport = value2.split(",")[1].trim();

								//sport
								if(CommonData.isaSport(sport)){
									//gender
									if(gender.equalsIgnoreCase("men")){
										gender = "M";
									}
									else if(gender.equalsIgnoreCase("women")){
										gender = "F";
									}

									m_sqlExpression = CommonData.formYESNOMainSQLStmt("athletes", "won", "sport", gender, sport, value3);
								}
							}

							else if(value2.contains("gold") || value2.contains("silver") || //win category sport
									value2.contains("bronze") ){
								if(value1.contains("won") || value1.contains("win")){
									m_sqlExpression = CommonData.formYESNOMainSQLStmt("athletes", "won", "sport", value2, value3);
								}
							}
							else  { //oscar
								 /*if(value2.contains("oscar")){ //win moviecategory year){
									String year = value3;
									m_sqlExpression = CommonData.formYESNOMainSQLStmt("oscar", "won", year);
								}
								else*/ if(value1.contains("win") && CommonData.isaYear(value3) ){
									m_sqlExpression = CommonData.formYESNOMainSQLStmt("oscar", "won", "");
									m_sqlExpression = lamdaReduction(m_sqlExpression, value3);
								}
							}
						}
					}
					else if(token1.contains("DT") && token2.contains("NNS") && token3.contains("NN")){
						//sports: the men slalom
						m_sqlExpression = m_childNodesList.get(1).getM_sqlExpression().trim() + "," + 
						m_childNodesList.get(2).getM_sqlExpression().trim();
					}
					else if(token1.contains("DT") && token2.contains("JJ") && token3.contains("NN")){
						String country = CommonData.getCountry(value2);
						String gender = value3.trim().toLowerCase();
						
						if(gender.matches("m[ea]n")){ //men or man
							gender = "M";
						}
						else if(gender.matches("wom[ea]n") ){ //woman or women
							gender = "F";
						}
						else
							gender = "NULL";

						m_sqlExpression = country + "," + gender;
					}
					else if(token2.contains("NP") && token3.contains("VP")){

						//win category year //Did Swank win the oscar in year?
						if(m_childNodesList.get(1).m_childNodesList.get(0).getM_nodeValue().equalsIgnoreCase("NNP")){
							String WINorSTAR = m_childNodesList.get(2).getchildNodesList().get(0).getM_attachment().trim();
							
							if(WINorSTAR.contains("win")){
								String WHERE = "WHERE P.NAME LIKE '%" +
								m_childNodesList.get(1).getM_sqlExpression() +
								"%'";
								m_sqlExpression += lamdaReduction( m_childNodesList.get(2).getM_sqlExpression(), WHERE );
							}
							else if(WINorSTAR.contains("star")){
								m_sqlExpression += lamdaReduction( m_childNodesList.get(2).getM_sqlExpression(), m_childNodesList.get(1).getM_sqlExpression() );
							}
						}
						else if(value2.contains(",")){ //win category sports
							String country = value2.split(",")[0].trim();
							String gender = value2.split(",")[1].trim();

							if(!gender.contains("NULL")) {
								m_sqlExpression += lamdaReduction(m_childNodesList.get(2).getM_sqlExpression(), country);
								m_sqlExpression = lamdaReduction(m_sqlExpression, gender);
							}
							else {//a French actor(France)
								String WHERE = "WHERE POB LIKE '%" + country +	"%'";
								m_sqlExpression += lamdaReduction(m_childNodesList.get(2).getM_sqlExpression(), WHERE);
							}
						}
						else
							m_sqlExpression += lamdaReduction( m_childNodesList.get(2).getM_sqlExpression(), 
									m_childNodesList.get(1).getM_sqlExpression() );
					}
					else
						m_sqlExpression += lamdaReduction( m_childNodesList.get(2).getM_sqlExpression(), 
								m_childNodesList.get(1).getM_sqlExpression() );
				}
				else if(CommonData.getM_qtype() == QUESTIONTYPE.WHQ){

					if( token1.contains("DT") && token2.contains("JJS") && token3.contains("NN") ||
							token1.contains("DT") && token2.contains("JJ") && token3.contains("NN") ){

						String value1 = m_childNodesList.get(1).getM_attachment();
						String value2 = m_childNodesList.get(2).getM_attachment();

						if( value1.equalsIgnoreCase("best") && value2.equalsIgnoreCase("movie") ||
								value1.matches("(gold)|(bronze)|(silver)") && value2.equalsIgnoreCase("medal") ){
							m_sqlExpression = value1 + "," + value2;
						}
						else{
							String attr;
							String tablename;

							//load db details
							CommonData.getDBTableRelationMap();
							CommonData.getTableAttrRelationMap();

							if( (tablename = CommonData.isaTable(value2)) != null){ //check NP in table
							
								if(tablename.contains("mountain")) 
									m_sqlExpression += "NAME FROM " + tablename;
								else //seas table
									m_sqlExpression += "OCEAN FROM " + tablename;
							}
							if( (attr = CommonData.isaTableAttr(value1, SynsetType.ADJECTIVE)) != null) //check JJS in attr	
								m_sqlExpression += CommonData.formJJSSQLAggFuncQry(tablename, attr, value1);
						}
					}
					else if( token1.contains("NNP") && token2.contains("NNP") && token3.contains("CD")){
						m_sqlExpression = m_childNodesList.get(0).getM_sqlExpression() + m_childNodesList.get(1).getM_sqlExpression() +
						m_childNodesList.get(2).getM_sqlExpression();
					}
					else if(token1.contains("VBD") && token2.contains("NP") && token3.contains("PP")){
						if(m_childNodesList.get(0).getM_attachment().contains("direct")){ //direct form
							String attrType =  m_childNodesList.get(1).getM_sqlExpression();
							m_sqlExpression = CommonData.formWHMainSQLStmt("direct", "direct" + attrType, m_childNodesList.get(2).getM_sqlExpression());
						}
						else if(m_childNodesList.get(0).getM_attachment().contains("won")){
							String attrType =  m_childNodesList.get(1).getM_sqlExpression();
							String year = m_childNodesList.get(2).getM_sqlExpression();

							//only for movie
							if(attrType.contains("BEST"))
								m_sqlExpression = CommonData.formWHMainSQLStmt("oscar", "won", attrType, year);
							else if(attrType.contains("oscar")){ //who won the oscar in year?
								m_sqlExpression = CommonData.formWHMainSQLStmt("oscar", "won", year);
							}
							else if(attrType.contains("medal")){ ////won+sportcategory+type+year+gender
								String award = attrType.trim();
								String sportType = m_childNodesList.get(2).getM_sqlExpression().split(",")[0].trim();
								String gender = m_childNodesList.get(2).getM_sqlExpression().split(",")[1].trim();
								String sport = m_childNodesList.get(2).getM_sqlExpression().split(",")[2].trim();
								String winyear = m_childNodesList.get(2).getM_sqlExpression().split(",")[3].trim();
								
								//medal
								if(award.contains("gold"))
									award = "gold";
								else if (award.contains("silver"))
									award = "silver";
								else if (award.contains("bronze"))
									award = "bronze";
								
								//remove 'm'
								sportType =  sportType.replace("m", "").trim();
								
								//gender
								if(gender.matches("m[ae]n"))
									gender = "M";
								else if(gender.matches("wom[ae]n"))
									gender = "F";
								
								m_sqlExpression = CommonData.formWHMainSQLStmt("athletes", "won", 
										award, sportType, gender, sport, winyear);							
							}
							else{ //won+category+year
								String gender =  attrType.split(",")[0].trim();
								String sport = attrType.split(",")[1].trim();
								String locationOrYear = year;

								if(!CommonData.isaYear(locationOrYear)) //no need to send vancouver(implicit)
									locationOrYear = "";
									
								if(gender.matches("m[ae]n"))
									gender = "M";
								
								else if(gender.matches("[w][o]m[ae]n"))
									gender = "F";
								
								//only for movie
								if(gender.matches("[MF]")) //men or man or woman or women
									m_sqlExpression = CommonData.formWHMainSQLStmt("athletes", "won", "vancouver", gender, sport, locationOrYear);

							}
						}
					}
					else if(token1.contains("JJS") && token2.contains("NN") && token3.contains("NN")){
						String value1 = m_childNodesList.get(0).getM_sqlExpression().trim();
						String value2 = m_childNodesList.get(1).getM_sqlExpression().trim();
						String value3 = m_childNodesList.get(2).getM_sqlExpression().trim();

						m_sqlExpression = lamdaReduction(value2, value3);

						if(value1.equalsIgnoreCase("best")){
							if(m_sqlExpression.contains("supportingactor"))
								m_sqlExpression = "BEST-SUPPORTING-ACTOR";
							else if(m_sqlExpression.contains("supportingactress"))
								m_sqlExpression = "BEST-SUPPORTING-ACTRESS";	
						}
					}
					else
						m_sqlExpression += lamdaReduction( m_childNodesList.get(1).getM_sqlExpression(), 
								m_childNodesList.get(2).getM_sqlExpression() );
				}
				else{
					// no need to have WH/IS(VBZ) and . sql expression
					m_sqlExpression += m_childNodesList.get(1).getM_sqlExpression();
				}
			}

			/*System.out.println("Root: " + m_nodeValue);
			System.out.println("---------------");
			System.out.println("Attachment: " + m_attachment);
			System.out.println("SQL" + m_sqlExpression);
			System.out.println("=================");*/

			return;
		}

		/*System.out.println("Root: " + m_nodeValue);
		System.out.println("---------------");
		System.out.println("Attachment: " + m_attachment);
		System.out.println("SQL" + m_sqlExpression);
		System.out.println("=================");*/
	}

	private QUESTIONTYPE identifyQuestionType(String token){

		QUESTIONTYPE qType = QUESTIONTYPE.INVALIDQ;

		if( token.equalsIgnoreCase("VBZ") || token.equalsIgnoreCase("VBD"))
			qType = QUESTIONTYPE.YESNO;
		else if( token.equalsIgnoreCase("WRB") || token.equalsIgnoreCase("WHADVP") || 
				token.equalsIgnoreCase("WHNP"))
			qType = QUESTIONTYPE.WHQ;

		return qType;
	}

	public String lamdaReduction(String node1, String node2){

		//remove ? if present
		node1 = node1.replace("?", "");
		node2 = node2.replace("?", "");

		String unifiedString = null;
		if(node1.contains(":X"))
			unifiedString = node1.replace(":X", node2.trim());
		else if(node2.contains(":X"))
			unifiedString = node2.replace(":X", node1.trim());
		else if(node1.contains(":Y"))
			unifiedString = node1.replace(":Y", node2.trim());	
		else if(node2.contains(":Y"))
			unifiedString = node2.replace(":Y", node1.trim());	
		else
			unifiedString = node1 + node2;

		return unifiedString;

	}

}
