package ui;

import semantics.SemAttachmentTree;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;

public class ToCustomStringLabeller extends ToStringLabeller<SemAttachmentTree> {
	
	public ToCustomStringLabeller() {
		// TODO Auto-generated constructor stub
	}
	
	public String transform(SemAttachmentTree tree){
		return /*"<html><font color=\"red\">"+*/tree.getM_nodeValue();
	}

}
