package ui;

import java.awt.Color;
import java.awt.Paint;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.JFrame;

import org.apache.commons.collections15.Transformer;

import semantics.SemAttachmentTree;
import semantics.SemanticAnalyzer;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.renderers.Renderer;

public class ParseTreeUI {

	CustomDelegateTree<SemAttachmentTree, String> m_ParseTreeUI = new CustomDelegateTree<SemAttachmentTree, String>();
	int m_edgeName = 0;

	public CustomDelegateTree<SemAttachmentTree, String> getParseTreeObject() {
		return m_ParseTreeUI;
	}

	public void drawFrame(){

		Layout<SemAttachmentTree, String> layout = new TreeLayout<SemAttachmentTree, String>(m_ParseTreeUI, 50, 50);

		VisualizationViewer<SemAttachmentTree, String> vv = 
			new VisualizationViewer<SemAttachmentTree, String>(layout);
		vv.getRenderContext().setLabelOffset(20); 
		vv.getRenderContext().setVertexLabelTransformer(new ToCustomStringLabeller());
		vv.getRenderContext().setVertexFillPaintTransformer( new Transformer<SemAttachmentTree, Paint>() {
			@Override
			public Paint transform(SemAttachmentTree arg0) {
				return Color.ORANGE;
			}
		});

		vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.S);
		//vv.getRenderContext().setEdgeShapeTransformer( new EdgeShape.Line<SemAttachmentTree, String>());
		//vv.setPreferredSize(new Dimension(200,200)); //Sets the viewing area size

		JFrame frame = new JFrame("Parse Tree View");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().add(vv);		

		//If pack is used no need to set PreferredSize(...)
		frame.pack();
		frame.setVisible(true);	
	}

	public void drawTree(){
		SemanticAnalyzer sa = SemanticAnalyzer.getInstance();

		//we should not call this here... we need to make sure the process of construction should be done before
		//sa.analyzeConstituents("which is the highest mountain in the world?");
		
		SemAttachmentTree root = sa.getM_SemAttachTree();
		
		//no need to display graph
		if(root.getchildNodesList().size() <= 0)
			return;
		
		Queue<SemAttachmentTree> queueTree = new LinkedList<SemAttachmentTree>();
		queueTree.add(root);

		while(!queueTree.isEmpty()){

			SemAttachmentTree subtree = queueTree.remove();

			//get the children nodes
			List<SemAttachmentTree> childList = subtree.getchildNodesList();

			//add to the queue
			queueTree.addAll(childList);

			/*//set up jung tree for parse tree
			String par = subtree.getM_nodeValue();
			List<String> childNodeValues = new LinkedList<String>();
			for(SemAttachmentTree child: childList){
				childNodeValues.add(child.getM_nodeValue());
			}*/

			//create parent-child relationship
			if(subtree.getM_nodeValue().contains("TOP"))
				m_ParseTreeUI.setRoot(subtree);

			for(SemAttachmentTree child: childList){
				m_ParseTreeUI.addChild("edge"+m_edgeName++, subtree, child);
			}

		}
	}

//	public static void main(String[] args) {
//		ParseTreeUI ptui = new ParseTreeUI();			
//		ptui.drawTree();
//		ptui.drawFrame();
//	}
}
