package main;

import java.sql.SQLException;
import java.util.List;

import category.CategoryIdentification;

import semantics.SQLQueryGenerator;
import semantics.SemanticAnalyzer;
import ui.ParseTreeUI;
import common.CommonData;
import common.CommonData.QUESTIONTYPE;
import common.CommonData.SQLTABLES;
import edu.uic.cs.cs421.miniwatson.IWatson;
import edu.uic.cs.cs421.miniwatson.QuestionCategory;
import edu.uic.cs.cs421.miniwatson.score.AutoScoreTool;

public class MiniWatsonApp implements IWatson{
	
	SemanticAnalyzer analyzer = null;
	
	String question;
	String sqlQuery;
	List<String> queryResult;
	
	public MiniWatsonApp() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String answerQuestion(String question) {	
		this.question = question;
		initApp();
		processQuery();
		return queryResult.toString().replace("[", "").replace("]", "");
	}

	@Override
	public QuestionCategory classifyCategory(String question) {
		this.question = question;
		initApp();
		
		/*SQLTABLES eDBType = CommonData.getDBType();
		
		if( SQLTABLES.OLYMPICS == eDBType )
			return QuestionCategory.OLYMPICS;
		else if( SQLTABLES.OSCAR == eDBType )
			return QuestionCategory.MOVIES;
		else if( SQLTABLES.GEOGRAPHY == eDBType )
			return QuestionCategory.GEOGRAPHY;
		else{*/
			
			CategoryIdentification cat = new CategoryIdentification();
			SQLTABLES dbType = cat.identifyQuestionType(question);
			
			if( SQLTABLES.OLYMPICS == dbType )
				return QuestionCategory.OLYMPICS;
			else if( SQLTABLES.OSCAR == dbType )
				return QuestionCategory.MOVIES;
			else if( SQLTABLES.GEOGRAPHY == dbType )
				return QuestionCategory.GEOGRAPHY;
		//}
		
		return null;
	}
	
	public String getSqlQuery() {
		return sqlQuery;
	}
	
	public List<String> getQueryResult() {
		return queryResult;
	}


	public MiniWatsonApp(String question) {
		this.question = question;
	}
	
	public void setQuestion(String question) {
		this.question = question;
		initApp();
	}
	
	/**
	 * Method to initialize the semantic analyser and generate SQL query
	 */
	public void initApp(){
		SemanticAnalyzer.deleteInstance();
		analyzer = SemanticAnalyzer.getInstance();
		analyzer.analyzeConstituents(question);
		sqlQuery = analyzer.getGeneratedsqlExpressionForQuery();
	}
	
	public void processQuery(){
		SQLQueryGenerator sqlgen = new SQLQueryGenerator();
		try {
			sqlgen.executeSQLExpression(CommonData.getDBType(), sqlQuery);
			
			//get the query result
			queryResult = sqlgen.getM_sQueryResponse();
			if(queryResult.isEmpty())
				queryResult.add("The answer is unknown");
	        
	        if(CommonData.getM_qtype() == QUESTIONTYPE.YESNO){
	        	Double result = Double.valueOf(queryResult.get(0).trim());
	        	if( result > 0 ){
	        		queryResult.clear();
	        		queryResult.add("Yes");
	        	}
	        	else{
	        		queryResult.clear();
	        		queryResult.add("No");
	        	}
	        }
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to draw the parse tree
	 */
	public void drawParseTree(){
		ParseTreeUI ui = new ParseTreeUI();
		ui.drawTree();
		ui.drawFrame();
	}
	
//	public static void main(String[] args){
//		MiniWatsonApp app = new MiniWatsonApp();
//		String res = app.answerQuestion("Is Rome the capital of Italy?");
//		QuestionCategory cat = app.classifyCategory("Is Rome the capital of Italy?");
//
//		System.out.println("Result: " + res);
//		System.out.println("Category: " + cat);
//
//		//AutoScoreTool scoretool = new AutoScoreTool(app);
//		//AutoScoreTool.main(args);
//	}
}
