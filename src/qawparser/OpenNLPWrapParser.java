package qawparser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.smu.tspell.wordnet.impl.file.Morphology;

import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.parser.chunking.Parser;
import opennlp.tools.parser.Parse;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;
import opennlp.tools.util.Span;

public class OpenNLPWrapParser extends AbstractParser {

	private InputStream m_tokenStream;
	private TokenizerModel m_tokenmodel;
	private TokenizerME m_tokenizer;

	private InputStream  m_parsein;
	ParserModel m_parsemodel;
	Parser m_qaparser;

	private InputStream  m_posin;
	POSModel m_posmodel;
	POSTaggerME m_postagger;
	
	static OpenNLPWrapParser m_ONLPParser;

	double gamesScore, movieScore, geographyScore;

	int PARSECOUNT;
	
	public Morphology m_morph;

	private OpenNLPWrapParser() {

		try {

			m_Listofquestions = new ArrayList<String>();

			PARSECOUNT = 4;

			//create a tokenizer
			m_tokenStream = new FileInputStream("./models/opennlp/en-token.bin");
			m_tokenmodel = new TokenizerModel(m_tokenStream);
			m_tokenizer =  new TokenizerME(m_tokenmodel);

			//create a parser
			m_parsein = new FileInputStream("./models/opennlp/en-parser-chunking.bin");
			m_parsemodel = new ParserModel(m_parsein);
			m_qaparser = (Parser) ParserFactory.create(m_parsemodel);

			//create a postagger
			m_posin = new FileInputStream("./models/opennlp/en-pos-maxent.bin");
			m_posmodel = new POSModel(m_posin);
			m_postagger = new POSTaggerME(m_posmodel);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static OpenNLPWrapParser getInstance(){
		if( null == m_ONLPParser)
			m_ONLPParser = new OpenNLPWrapParser();	
		return m_ONLPParser;
	}

	@Override
	public void loadquestions() {
		String filename = "./db/samplequestions.txt";
		String questionLine;
		try {
			BufferedReader reader =  new BufferedReader(new FileReader(filename));

			while(( questionLine = reader.readLine())!=null)
				m_Listofquestions.add(questionLine);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public HashMap<String, String[]> parseSentences() {

		HashMap<String, String[]> totalparseMap = new HashMap<String, String[]>();
		String[] parseStr = null;

		//list of token array
		for(String s: m_Listofquestions)
		{
			parseStr = parseSentence(s);

			//print the parseArray
						for(int i = 0; i < parseStr.length;i++)
						{
							System.out.println("\nThe " + String.valueOf(i+1) + " parse tree is:: ");
							System.out.println(parseStr[i]);
						}

			//append the sentences to the list
			totalparseMap.put(s, parseStr);

			System.out.println("------------------------------------------- ");
		}

		return totalparseMap;
	}

	public String[] tokenizeSentence(String s){
		String[] tokens = m_tokenizer.tokenize(s);
		return tokens;		
	}

	@Override
	public String[] parseSentence(String s) {

		//tokenize sentence
		String tokenedSentence =  new String();
		String[] tokens = tokenizeSentence(s);

		for (int j = 0; j < tokens.length; j++)
		{
			tokenedSentence += tokens[j];
			tokenedSentence += " ";
		}

		tokenedSentence.trim();
		
		//generate parse trees
		Parse[] parseArray = null;
		parseArray = ParserTool.parseLine(tokenedSentence, m_qaparser, PARSECOUNT);

		//for(Parse parse: parseArray)
		//parse.show();
		//double max = 0;
		//int maxIndex = 0;
		String[] parseStr = new String[PARSECOUNT];
		for(int i = 0; i < parseArray.length; i++)
		{
			//if( parseArray[i].getProb() > max )
			//{
			//max = parseArray[i].getProb();
			//maxIndex = i;
			StringBuffer buffer = new StringBuffer();
			parseArray[i].show(buffer);
			parseStr[i] = buffer.toString();
			//}
		}

		//		StringBuffer buffer = new StringBuffer();
		//		parseArray[maxIndex].show(buffer);
		//		parseStr[0] = buffer.toString();

		return parseStr; 
	}

	@Override
	public
	Parse[] parseSentence1(String s) {
		
		//tokenize sentence
		String tokenedSentence =  new String();
		String[] tokens = tokenizeSentence(s);

		for (int j = 0; j < tokens.length; j++)
		{
			tokenedSentence += tokens[j];
			tokenedSentence += " ";
		}

		tokenedSentence.trim();
		
		//generate parse trees
		return ParserTool.parseLine(tokenedSentence, m_qaparser, PARSECOUNT);
	}	
	
	public String[] POStagSentence(String[] sent){
		return m_postagger.tag(sent);
	}

	private List<String> identifyPOStags(String[] tagsOfSentence, String[] tokenedSent, List<String> POStags){
		
		//extract the POS tags and generate the score from the table data
		List<Integer> POSIndexes = new ArrayList<Integer>();

		for(String POS: POStags){
			for(int i = 0; i < tagsOfSentence.length; i++){
				if( tagsOfSentence[i].contains(POS) )
					POSIndexes.add(i);
			}
		}

		//get all the noun forms in the sentence
		List<String> POSWords = new ArrayList<String>();
		for(int index: POSIndexes)
			POSWords.add(tokenedSent[index]);
	
		return POSWords;
	}
	
	public List<String> getPOSWordsofSentence(String sent, List<String> POStags){

		//tokenize and get the POS tags
		String[] tokenedSent = tokenizeSentence(sent);
		String[] tagsOfSentence = POStagSentence(tokenedSent);

		List<String> POSWords = identifyPOStags(tagsOfSentence, tokenedSent, POStags);
		return POSWords;
	}

	public void findNamedEntities() {

		ArrayList<InputStream> models = new ArrayList<InputStream>(); 
		List<String> questions = getListofquestions();
		//questions.add("Who directed Hugo");
		questions.clear();
		questions.add("Korean");

		// Read the model file
		InputStream m_personStream = null;
		InputStream m_dateStream = null;
		InputStream m_locationStream = null;

		try {
			m_personStream = new FileInputStream("./models/opennlp/en-ner-person.bin");
			models.add(m_personStream);
			m_dateStream = new FileInputStream("./models/opennlp/en-ner-date.bin");
			models.add(m_dateStream);
			m_locationStream = new FileInputStream("./models/opennlp/en-ner-location.bin");
			models.add(m_locationStream);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		//Person Model
		TokenNameFinderModel personModel = null;
		TokenNameFinderModel dateModel = null;
		TokenNameFinderModel locationModel = null;
		try {
			personModel = new TokenNameFinderModel(
					m_personStream);
			dateModel = new TokenNameFinderModel(
					m_dateStream);
			locationModel = new TokenNameFinderModel(
					m_locationStream);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		NameFinderME personFinder = new NameFinderME(personModel);
		NameFinderME dateFinder = new NameFinderME(dateModel);
		NameFinderME locationFinder = new NameFinderME(locationModel);

		for(String query:questions){
			gamesScore = movieScore = geographyScore = 0;

			String[] tokens = tokenizeSentence(query);

			Span personNames[] = personFinder.find(tokens);
			Span dateNames[] = dateFinder.find(tokens);
			Span locationNames[] = locationFinder.find(tokens);

			for(Span span:personNames){
				for(int i = span.getStart(); i < span.getEnd(); i++){
					gamesScore = gamesScore + 0.3;
					movieScore = movieScore + 0.6;
					geographyScore = geographyScore + 0.1;
				}
			}

			for(Span span:dateNames){
				for(int i = span.getStart(); i < span.getEnd(); i++){
					if(tokens[i].equals("2010"))
						gamesScore = gamesScore + 0.8;
					movieScore = movieScore + 0.2;
				}
			}

			for(Span span:locationNames){
				for(int i = span.getStart(); i < span.getEnd(); i++){
					geographyScore = geographyScore + 0.9;
					gamesScore = gamesScore + 0.1;
				}
			}

			System.out.println("--For question:[" + query + "] the domain scores are:");
			System.out.println("Games:" + gamesScore + " " + "Movies:" + movieScore + " " + "Location:" + geographyScore);

		}

		try {
			m_dateStream.close();
			m_personStream.close();
			m_locationStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writecsvfile( HashMap<String, String[]> parseHash){
		try {
			OutputStream os = new FileOutputStream("ParseTree.txt");
			String sData = new String();
			int count = 0;
			for(String sent: m_Listofquestions){
				count++;
				if(parseHash.containsKey(sent)){
					String[] parseTrees = parseHash.get(sent);
					sData += count + ") " + sent + "\n";
					sData += "-----------------------------------------------------------------------------------------\n";
					sData += "Parse 1: " + parseTrees[0] + "\n" +  "Parse 2: "+ parseTrees[1] + "\n";
					sData +=   "Parse 3: "+ parseTrees[2] + "\n" +  "Parse 4: "+ parseTrees[3] + "\n";
					sData += "==========================================================================================\n\n";
				}
			}

			os.write(sData.getBytes());
			os.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String curdir = System.getProperty("user.dir");

		AbstractParser nlpParser = new OpenNLPWrapParser();
		OpenNLPWrapParser parser = (OpenNLPWrapParser)nlpParser;
		//parser.loadquestions();
		parser.findNamedEntities();
		//parser.writecsvfile(nlpParser.parseSentences());
	}
}
