package qawparser;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import opennlp.tools.parser.Parse;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.objectbank.TokenizerFactory;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreePrint;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

public class StanfordWrapParser extends AbstractParser {

	LexicalizedParser m_lexicalparser;
	TokenizerFactory<CoreLabel> m_tokenizerFactory;
	
	public StanfordWrapParser() {
		m_lexicalparser = new LexicalizedParser("./models/stanfordnlp/englishPCFG.ser.gz");
		m_tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
		m_Listofquestions = new ArrayList<String>();
	}
	
	@Override
	void loadquestions() {
		super.loadquestions();
	}

	@Override
	public
	HashMap<String, String[]> parseSentences() {
		return super.parseSentences();
	}

	@Override
	String[] parseSentence(String s) {

		//courtesy: stanford examples
			
		//tokenizing the sentence
		List<CoreLabel> words = m_tokenizerFactory.getTokenizer(new StringReader(s)).tokenize();
		Tree parse = m_lexicalparser.apply(words);

		//parse tree print
		TreePrint treeprint = new TreePrint("penn,typedDependenciesCollapsed");
		treeprint.printTree(parse); //printing on the console
		
		//convert tree to string
		String[] parseStr =  new String[1];
		parseStr[0] = new String();
		
		OutputStream os = new ByteArrayOutputStream();
		PrintWriter pw = new PrintWriter(os);
			
		treeprint.printTree(parse, pw);
		parseStr[0] = os.toString();
		
		return parseStr;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AbstractParser stan = new StanfordWrapParser();
		stan.loadquestions();
		stan.parseSentence("Which is the highest mountain in the world?");
		//stan.parseSentences();
	}

	@Override
	Parse[] parseSentence1(String s) {
		// TODO Auto-generated method stub
		return null;
	}

}
