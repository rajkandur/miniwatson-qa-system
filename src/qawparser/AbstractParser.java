package qawparser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import opennlp.tools.parser.Parse;

/**
 * @author kandur
 *
 */
public abstract class AbstractParser {
	
	List<String> m_Listofquestions;
	
	void loadquestions(){
		String filename = ".\\db\\samplequestions.txt";
		String questionLine;
		try {
			BufferedReader reader =  new BufferedReader(new FileReader(filename));

			if(m_Listofquestions == null)
				return;
			
			while(( questionLine = reader.readLine())!=null)
				m_Listofquestions.add(questionLine);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @return
	 * Returns the syntactic parse tree array for the given set of questions
	 */
	public HashMap<String, String[]> parseSentences(){

		HashMap<String, String[]> totalparseMap = new HashMap<String, String[]>();
		String[] parseStr = null;
		
		//list of token array
		for(String s: m_Listofquestions)
		{
			parseStr = parseSentence(s);

			//print the parseArray
			for(int i = 0; i < parseStr.length;i++)
			{
				System.out.println("\nThe " + String.valueOf(i+1) + " parse tree is:: ");
				System.out.println(parseStr[i]);
			}
		
			//append the sentences to the list
			totalparseMap.put(s, parseStr);
			
			System.out.println("------------------------------------------- ");
		}

		return totalparseMap;
	}
	
	/**
	 * @return
	 * Returns the syntactic parse tree for the given question
	 */
	abstract String[] parseSentence(String s);
	
	/**
	 * @return
	 * Returns the syntactic parse tree for the given question
	 */
	abstract Parse[] parseSentence1(String s);

	public List<String> getListofquestions() {
		return m_Listofquestions;
	}

	public void setListofquestions(List<String> m_Listofquestions) {
		this.m_Listofquestions = m_Listofquestions;
	}
}
