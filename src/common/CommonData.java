package common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import category.WordNetUtils;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.impl.LeacockChodorow;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
import edu.smu.tspell.wordnet.SynsetType;

public class CommonData {

	private static ILexicalDatabase m_db = new NictWordNet();

	public enum SQLTABLES
	{
		OLYMPICS, //sports
		OSCAR, //movie
		GEOGRAPHY, //geo
		INVALID
	};

	public enum QUESTIONTYPE{
		DECL, //Declarative
		IMP, //Imperative
		YESNO, //Yes-no //now used 
		WHQ, // Wh question type //now used
		INVALIDQ
	};

	public enum CONSTITUENTTYPE{
		CHUNK, // NP, VP, WDT, WHNP
		POS // verb, noun, adj, advp 
	}

	public enum SQLCOMMANDTYPE{
		SELECT,
		FROM,
		WHERE,
		TEXT //simple text/word form
	}

	public enum PREPTYPE{
		OF,
		IN,
		THAN
	}

	//SQL max function keywords
	private static String[] m_MaxFuncArray = {"highest", "longest", "deepest", "widest", "tallest"};
	public static List<String> MAXFUNC = Arrays.asList(m_MaxFuncArray);

	//SQL min function keywords
	private static String[] m_MinFuncArray = {"lowest", "shortest", "shallowest", "minute", "smallest"};
	public static List<String> MINFUNC = Arrays.asList(m_MinFuncArray);

	//hashset mapping the table and its DB
	private static HashMap<SQLTABLES, List<String>> m_DBTablesRelMap = null;

	//hashset mapping the table and its attributes
	private static HashMap<String, List<String>> m_TableAttrRelMap = null;

	private static HashSet<String> m_yearValuesSet = null;

	private static SQLTABLES m_eDBType = SQLTABLES.INVALID;

	private static QUESTIONTYPE m_qtype = QUESTIONTYPE.INVALIDQ;

	private static PREPTYPE m_prepYesNoType;

	/**
	 * updates the relations map
	 */
	private static void loadDBRelations(){
		SQLTABLES[] tableArray = { SQLTABLES.OLYMPICS, SQLTABLES.OSCAR, SQLTABLES.GEOGRAPHY };
		List<SQLTABLES> listtables = new ArrayList<SQLTABLES>();
		listtables.addAll(Arrays.asList(tableArray));

		//olympics, movie and geo tables array
		String[] olympicstables = { "results", "athletes", "competitions"};
		String[] movietables = { "movie", "person", "actor", "director", "oscar"};
		String[] geotables = { "cities", "capitals", "borders", "seas", "continents", "countries", "countrycontinents", "mountains"};

		//DB - table relations
		m_DBTablesRelMap.put(SQLTABLES.OLYMPICS, Arrays.asList(olympicstables));
		m_DBTablesRelMap.put(SQLTABLES.OSCAR, Arrays.asList(movietables));
		m_DBTablesRelMap.put(SQLTABLES.GEOGRAPHY, Arrays.asList(geotables));

		//Table - attributes: geo table
		m_TableAttrRelMap.put("cities", Arrays.asList(new String[]{"id", "name"}));
		m_TableAttrRelMap.put("capitals", Arrays.asList(new String[]{"countryid", "cityid"}));
		m_TableAttrRelMap.put("borders", Arrays.asList(new String[]{"country1", "country2"}));
		m_TableAttrRelMap.put("seas", Arrays.asList(new String[]{"id", "ocean", "deepest"}));
		m_TableAttrRelMap.put("continents", Arrays.asList(new String[]{"id", "continent", "area_km2", "population", "highest", "lowest"}));
		m_TableAttrRelMap.put("countries", Arrays.asList(new String[]{"id", "name"}));
		m_TableAttrRelMap.put("countrycontinents", Arrays.asList(new String[]{"countryid", "continentid"}));
		m_TableAttrRelMap.put("mountains", Arrays.asList(new String[]{"id", "name", "height"}));

		//: movie table
		m_TableAttrRelMap.put("movie", Arrays.asList(new String[]{"id", "name", "year", "rating", "runtime", "genre", "earnings_rank"}));
		m_TableAttrRelMap.put("person", Arrays.asList(new String[]{"id", "name", "dob", "pob"}));
		m_TableAttrRelMap.put("actor", Arrays.asList(new String[]{"actor_id", "movie_id"}));
		m_TableAttrRelMap.put("director", Arrays.asList(new String[]{"director_id", "movie_id"}));
		m_TableAttrRelMap.put("oscar", Arrays.asList(new String[]{"movie_id", "person_id", "type", "year"}));

		//: olympics table
		m_TableAttrRelMap.put("results", Arrays.asList(new String[]{"comp_id", "winner", "medal", "year"}));
		m_TableAttrRelMap.put("athletes", Arrays.asList(new String[]{"name", "nationality", "gender"}));
		m_TableAttrRelMap.put("competitions", Arrays.asList(new String[]{"comp_id", "name", "type"}));
	}


	public static HashMap<SQLTABLES, List<String>> getDBTableRelationMap(){
		if(m_DBTablesRelMap == null){
			m_DBTablesRelMap = new HashMap<SQLTABLES, List<String>>();

			//call only once
			if( m_TableAttrRelMap == null){
				m_TableAttrRelMap = new HashMap<String, List<String>>();
				loadDBRelations();
			}
		}

		return m_DBTablesRelMap;
	}

	public static HashMap<String, List<String>> getTableAttrRelationMap(){
		if(m_TableAttrRelMap == null){
			m_TableAttrRelMap = new HashMap<String, List<String>>();

			//call only once
			if( m_DBTablesRelMap == null){
				m_DBTablesRelMap = new HashMap<SQLTABLES, List<String>>();
				loadDBRelations();
			}
		}

		return m_TableAttrRelMap;
	}

	public static double getSemanticSimilarity(String source, String target){
		RelatednessCalculator[] rcs = {
				/*new HirstStOnge(m_db),*/ new LeacockChodorow(m_db)/*,  new Lesk(m_db),  new WuPalmer(m_db)/*, 
			/* new Resnik(m_db)/*,/* new JiangConrath(m_db), new Lin(m_db),*/ /*new Path(m_db)*/
		};

		double s = 0.0;
		WS4JConfiguration.getInstance().setMFS(true);
		for ( RelatednessCalculator rc : rcs ) {
			s = rc.calcRelatednessOfWords(source, target);
			System.out.println( rc.getClass().getName()+"\t"+s );
		}

		return s;
	}

	public static boolean isaYear(String yeardata){
		/*if(m_yearValuesSet == null ){
			m_yearValuesSet = new HashSet<String>();
			m_yearValuesSet.addAll(Arrays.asList(new String[]{"2006", "2007", "2008", "2009", "2010"}));
		}*/

		try{
			int year = Integer.valueOf(yeardata.trim()).intValue();
			if(year <= 1900 && year >= 2100) return false;
			else return true;
		}
		catch (Exception e) {
			return false;
		}

		//check whether it is a year
		//return m_yearValuesSet.contains(yeardata);
	}

	public static boolean isaSport(String sportname){

		List<String> m_PossibleSports = new ArrayList<String>();
		m_PossibleSports.addAll(Arrays.asList(new String[]{"slalom", "biathlon", 
				"skiijumping", "giantslalom",
				"crosscountry", "shorttrack",
				"speedskating", "figureskating"}));



		return m_PossibleSports.contains(sportname.toLowerCase());
	}

	public static String isaTableAttr(String attr, SynsetType type){

		//retrieve only those values which is of the selected DB earlier
		Set<String> keys = m_TableAttrRelMap.keySet();
		List<String> values = new LinkedList<String>();
		for(String key: keys){
			for(String keyvalue: m_TableAttrRelMap.get(key))
				if(!keyvalue.matches("^.*id$") && //not adding id
						!keyvalue.matches("^.*name$") ) //not adding name
					values.add(keyvalue);
		}

		/*for(String key: keys){
			if( 0.4 <= getSemanticSimilarity(key, attr) ) return key;
		}*/

		//get the lemma of this adjective with table attributes of the table
		WordNetUtils utils = WordNetUtils.getinstance();
		String lemma[] = utils.getLemma(attr, type);

		String lemmatizedWord = null;
		if(lemma.length == 0)
		{
			lemmatizedWord = new String();
			lemmatizedWord = attr;
		}
		else
			lemmatizedWord = lemma[0];		

		//check the words which are directly related to the table attribute like: height - high
		String actualAttr = null;
		for( String value: values){
			/*double sim = getSemanticSimilarity(value, lemmatizedWord);

			//get the attribute/column name
			if(0.5 <= sim)*/
			if( value.contains(lemmatizedWord) || utils.isaAttribute(value, lemmatizedWord)){
				actualAttr = value;
				break;
			}
		}

		//check whether its antonyms present in table-attributes
		//check whether the table-attributes' instance is this word


		return actualAttr;
	}

	private static String identifyTable(String attr){

		//retrieve only those values which is of the selected DB earlier
		Set<String> keys = m_TableAttrRelMap.keySet();
		List<String> values = new LinkedList<String>();
		for(String key: keys){
			values.clear();
			for(String keyvalue: m_TableAttrRelMap.get(key))
				if(!keyvalue.matches("^.*id$") && //not adding id
						!keyvalue.matches("^.*name$") ) //not adding name
					values.add(keyvalue);

			if( values.contains(attr))
				return key; //found table
		}

		return null; //no table identified
	}

	public static String isaTable(String table/* or attr *//*, SynsetType... type*/){

		if( null == m_DBTablesRelMap || m_DBTablesRelMap.isEmpty())
			return null;

		Set<SQLTABLES> keys = m_DBTablesRelMap.keySet();

		/*for(SQLTABLES key: keys){
			if( 0.4 <= getSemanticSimilarity(key.toString().toLowerCase(), table) ) return key.toString().toLowerCase();
		}*/

		//get the lemma of this adjective with table attributes of the table

		/*String lemmatizedWord = null;
		if(type.length > 0){
			WordNetUtils utils = WordNetUtils.getinstance();
			String lemma[] = utils.getLemma(table, type[0]);

			if(lemma.length == 0)
			{
				lemmatizedWord = new String();
				lemmatizedWord = table;
			}
			else
				lemmatizedWord = lemma[0];	
		}
		else
			lemmatizedWord = table;*/	

		for(SQLTABLES DB: keys ){
			List<String> tablenames = m_DBTablesRelMap.get(DB);
			for(String key: tablenames){
				if(key.contains(table))	
					return key;
			}
		}	

		//check whether this attribute is a part of any table
		String foundTable = identifyTable(table);
		return foundTable;
	}

	public static String formJJSSQLAggFuncQry(String tablename, String attr, String token){

		//get the functions: max or min
		String sqlExpression = new String();
		if( MAXFUNC.contains(token)){
			sqlExpression = " WHERE " + attr + " = (SELECT MAX(" + attr + ") FROM "+ tablename + ")"; 
		}
		else if(MINFUNC.contains(token)){
			sqlExpression = " WHERE " + attr + " = (SELECT MIN(" + attr + ") FROM "+ tablename + ")";
		}

		//set the db type
		for(SQLTABLES DB: m_DBTablesRelMap.keySet() ){
			List<String> tablenames = m_DBTablesRelMap.get(DB);
			if(tablenames.contains(tablename)){
				m_eDBType = DB;
				break;
			}
		}

		return sqlExpression;	
	}

	public static String formWHMainSQLStmt(String tablename, String token, String... attr){

		String sqlExpression = new String();
		boolean bCheckDbName = false;

		if(token.contains("capital")){
			sqlExpression = "CS.NAME FROM " + tablename + " C " +
			"INNER JOIN COUNTRIES CN ON CN.ID = C.COUNTRYID " +
			"INNER JOIN CITIES CS on CS.ID = C.CITYID " +
			"WHERE CN.NAME LIKE '%:X%'";

			bCheckDbName = true;
		}
		else if(token.contains("direct")){

			if( attr == null || attr[0].isEmpty()){
				sqlExpression = "P.NAME FROM PERSON P " +
				"INNER JOIN DIRECTOR D ON D.DIRECTOR_ID=P.ID " +
				"INNER JOIN MOVIE M ON M.ID=D.MOVIE_ID " +
				"WHERE M.NAME LIKE '%:X%'";

				bCheckDbName = true;
			}
			else if(token.contains("best")){
				if(token.contains("movie")){
					token = "BEST-PICTURE";			
					sqlExpression = "P.NAME FROM MOVIE M " +
					"INNER JOIN DIRECTOR D ON M.ID=D.MOVIE_ID " +
					"INNER JOIN PERSON P ON P.ID=D.DIRECTOR_ID " +
					"INNER JOIN OSCAR O ON O.MOVIE_ID=M.ID " +
					"WHERE O.TYPE LIKE '%" + token +
					"%' AND O.YEAR LIKE'%" + attr[0].trim() + "%'";	

					bCheckDbName = true;
				}
			}
		}
		else if(token.contains("won")){ //best actor, actress, director, supporting actor and actress
			if(attr != null && attr.length == 1){ // who won the oscar in year?
				sqlExpression = " P.NAME FROM OSCAR O " +
				"INNER JOIN PERSON P ON P.ID=O.PERSON_ID " +
				"WHERE O.YEAR=" + attr[0] + "";
			}
			else if( attr != null && attr.length == 2){
				sqlExpression = "P.NAME FROM OSCAR O " +
				"INNER JOIN PERSON P ON P.ID=O.PERSON_ID " +
				"WHERE O.TYPE LIKE " + "'%" + attr[0].trim() + "%' AND " + "O.YEAR LIKE '%" + attr[1].trim() + "%'";
			}
			else if( attr != null && attr.length == 4){ //won+category+[location]+[year]
				if(attr[0].equalsIgnoreCase("vancouver")){
					sqlExpression = " * FROM ATHLETES A " +
					"INNER JOIN RESULTS R ON R.WINNER=A.NAME " +
					"INNER JOIN COMPETITIONS C ON C.COMP_ID=R.COMP_ID " +
					"WHERE A.GENDER='" + attr[1] + "' AND C.NAME LIKE '%" +
					attr[2] + "%' " +
					"AND R.YEAR LIKE '%" + attr[3] + "%'";
				}
			}
			else if(attr != null && attr.length == 5){ ////won+sportcategory+type+year+gender
				sqlExpression = " A.NAME FROM ATHLETES A " +
				"INNER JOIN RESULTS R ON R.WINNER=A.NAME " +
				"INNER JOIN COMPETITIONS C ON C.COMP_ID=R.COMP_ID " +
				"WHERE A.GENDER='" + attr[2] + "' AND C.NAME LIKE '%" +
				attr[3] + "%' " + "AND C.TYPE = '" +
				attr[1] + "' AND R.YEAR LIKE '%" +
				attr[4] + "%' AND R.MEDAL = '" + attr[0] +	"'" ;
			}

			bCheckDbName = true;
		}

		if(bCheckDbName){
			//set the db type
			for(SQLTABLES DB: m_DBTablesRelMap.keySet() ){
				List<String> tablenames = m_DBTablesRelMap.get(DB);
				if(tablenames.contains(tablename.toLowerCase())){
					m_eDBType = DB;
					break;
				}
			}
		}

		return sqlExpression;
	}

	public static String formYESNOMainSQLStmt(String tablename, String token, String... attr){

		String sqlExpression = new String();
		boolean bCheckDbName = false;

		if(token.contains("capital")){
			sqlExpression = " FROM " + tablename + " C " +
			"INNER JOIN COUNTRIES CN ON CN.ID = C.COUNTRYID " +
			"INNER JOIN CITIES CS on CS.ID = C.CITYID " +
			"WHERE CN.NAME LIKE '%:X%' AND " +
			"CS.NAME LIKE '%:Y%'";

			bCheckDbName = true;
		}
		else if(token.contains("countrycontinents")){
			sqlExpression = " FROM " + tablename + " C " +
			"INNER JOIN COUNTRIES CN ON CN.ID = C.COUNTRYID " +
			"INNER JOIN CONTINENTS CS on CS.ID = C.CONTINENTID " +
			"WHERE CS.CONTINENT LIKE '%:X%' AND " +
			"CN.NAME LIKE '%:Y%'";

			bCheckDbName = true;
		}
		else if(token.contains("seas")){
			sqlExpression = " FROM " + tablename + " OC";

			if( attr[0].contains("deep"))
				sqlExpression += " WHERE OC.DEEPEST > (SELECT DEEPEST FROM SEAS OC WHERE OC.OCEAN LIKE '%:Y%') AND"; 
			else if( attr[0].contains("shallow"))
				sqlExpression += " WHERE OC.DEEPEST < (SELECT DEEPEST FROM SEAS OC WHERE OC.OCEAN LIKE '%:Y%') AND"; 

			sqlExpression += " OC.OCEAN LIKE '%:X%'";

			bCheckDbName = true;
		}
		else if(token.contains("won")){ //best actor, actress, director, supporting actor and actress
			//sport competition
			if(attr != null && attr[0].contains("sport")){			
				if(attr.length == 4){
					sqlExpression = "FROM RESULTS R " +
					"INNER JOIN COMPETITIONS C ON C.COMP_ID=R.COMP_ID " +
					"INNER JOIN ATHLETES A ON A.NAME LIKE '%:Y%' AND A.GENDER = '" +
					attr[1] + "' " +
					"WHERE R.YEAR=" +
					attr[3] +
					" AND R.WINNER LIKE '%:Y%' " +
					"AND C.NAME LIKE '%" + attr[2] + "%'";
				}
				else if(attr.length == 3){
					sqlExpression = "FROM ATHLETES A " +
					"INNER JOIN RESULTS R ON R.WINNER=A.NAME " +
					"INNER JOIN COMPETITIONS C ON C.COMP_ID=R.COMP_ID " +
					"WHERE A.NATIONALITY LIKE'%:X%' AND A.GENDER=':Y' AND R.MEDAL = '" +
					attr[1] + "' " +
					"AND C.NAME LIKE '%" + attr[2] + "%'" ;			
				}
			}
			else{ //Did Swank won the oscar... or //Did a French actor win...
				sqlExpression = "FROM OSCAR O " +
				"INNER JOIN PERSON P ON P.ID=O.PERSON_ID " +
				":Y " +
				"AND O.YEAR LIKE '%:X%'";
			}

			bCheckDbName = true;
		}
		else if(token.contains("star")){ //Did Neeson star in movie name?

			sqlExpression = " FROM MOVIE M " +
			"INNER JOIN PERSON P ON P.ID=A.ACTOR_ID " +
			"INNER JOIN ACTOR A ON A.MOVIE_ID=M.ID " +
			"WHERE  P.NAME LIKE '%:Y%' AND M.NAME LIKE '%:X%'";
			
			bCheckDbName = true;
		}

		if(bCheckDbName){
			//set the db type
			for(SQLTABLES DB: m_DBTablesRelMap.keySet() ){
				List<String> tablenames = m_DBTablesRelMap.get(DB);
				if(tablenames.contains(tablename.toLowerCase())){
					m_eDBType = DB;
					break;
				}
			}
		}

		return sqlExpression;	
	}

	public static SQLTABLES getDBType(){
		return m_eDBType;
	}

	public static void setM_qtype(QUESTIONTYPE qtype) {
		m_qtype = qtype;
	}

	public static QUESTIONTYPE getM_qtype() {
		return m_qtype;
	}

	public static PREPTYPE getM_prepYesNoType() {
		return m_prepYesNoType;
	}

	public static void setM_prepYesNoType(PREPTYPE m_prepYesNoType) {
		CommonData.m_prepYesNoType = m_prepYesNoType;
	}

	public static String getCountry(String instance){
		WordNetUtils utils = WordNetUtils.getinstance();
		String country = utils.getCountryFromInstance(instance);

		if(country == null) 
			return instance;
		else{
			if( country.contains("United States"))
				country = "USA";
			else if(country.contains("Great Britain"))
				country = "UK";

			return country;
		}
	}
	
	public static boolean isContinent(String continent){
		WordNetUtils utils = WordNetUtils.getinstance();
		boolean bStatus = utils.bAreinstanceHypernyms(continent, "continent", SynsetType.NOUN);	
		return bStatus;
	}
	
	public static boolean isCountry(String continent){
		WordNetUtils utils = WordNetUtils.getinstance();
		boolean bStatus = utils.bAreinstanceHypernyms(continent, "country", SynsetType.NOUN);	
		return bStatus;
	}
}