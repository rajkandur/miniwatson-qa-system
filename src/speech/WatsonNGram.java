/*
 * Copyright 1999-2004 Carnegie Mellon University.
 * Portions Copyright 2004 Sun Microsystems, Inc.
 * Portions Copyright 2004 Mitsubishi Electric Research Laboratories.
 * All Rights Reserved.  Use is subject to license terms.
 *
 * See the file "license.terms" for information on usage and
 * redistribution of this file, and for a DISCLAIMER OF ALL
 * WARRANTIES.
 *
 */

package speech;

import main.MiniWatsonApp;
import tts.SpeakText;
import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;


/**
 * A simple HelloNGram demo showing a simple speech application built using Sphinx-4. This application uses the Sphinx-4
 * endpointer, which automatically segments incoming audio into utterances and silences.
 */
public class WatsonNGram {

	ConfigurationManager cm;

	public WatsonNGram() {
		cm = new ConfigurationManager(WatsonNGram.class.getResource("watsonngram.config.xml"));	
	}
	
	public String configureAndgetText(MiniWatsonApp runApp ){

		// allocate the recognizer
		System.out.println("Loading...");
		Recognizer recognizer = (Recognizer) cm.lookup("recognizer");
		recognizer.allocate();

		// start the microphone or exit if the programm if this is not possible
		Microphone microphone = (Microphone) cm.lookup("microphone");
		if (!microphone.startRecording()) {
			System.out.println("Cannot start microphone.");
			recognizer.deallocate();
			System.exit(1);
		}

		printInstructions();

		// loop the recognition until the programm exits.
		//while (true) {
			System.out.println("Start speaking. Press Ctrl-C to quit.\n");

			Result result = recognizer.recognize();
			String resultText = null;

			if (result != null) {
				resultText = result.getBestResultNoFiller();
				System.out.println("You said: " + resultText + '\n');
			} else {
				System.out.println("I can't hear what you said.\n");
			}

			if(runApp != null || runApp.getQueryResult() != null){
				new SpeakText(resultText, "kevin16");
			}
		//}

		return resultText;
	}

	/** Prints out what to say for this demo. */
	private static void printInstructions() {
		System.out.println("Allowed sentences:\n" +
				"Who directed Hugo\n" +
				"Who directed Shrek\n" +
				"What is the capital of Italy\n" +
				"What is the capital of France\n" +
				"Who directed Titanic\n" +
				"Is Rome in Italy\n" +
				"Is France in Europe\n" +
				"Is the Pacific deeper than the Atlantic\n"
		);
	}
}
