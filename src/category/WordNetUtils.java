package category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import qawparser.OpenNLPWrapParser;
import edu.smu.tspell.wordnet.AdjectiveSynset;
import edu.smu.tspell.wordnet.AdverbSynset;
import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.VerbSynset;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.smu.tspell.wordnet.WordSense;
import edu.smu.tspell.wordnet.impl.file.Morphology;
import edu.smu.tspell.wordnet.impl.file.synset.AdjectiveSatelliteReferenceSynset;

public class WordNetUtils {

	public WordNetDatabase wordNet;
	private PorterStemmer m_PStemmer; // just a upper level class for underlying Standard Stemmer Algorithm
	OpenNLPWrapParser m_Parser;
	Morphology m_morph;
	HashMap<String, String> m_hashRepeatedStr;
	private static WordNetUtils m_WNutils;

	private WordNetUtils(){
		initWordnet();

		m_PStemmer = new PorterStemmer();
		m_Parser = OpenNLPWrapParser.getInstance();
		m_morph = Morphology.getInstance();
		m_hashRepeatedStr = new HashMap<String, String>();
	}

	public static WordNetUtils getinstance(){
		if(null == m_WNutils)
			m_WNutils = new WordNetUtils();

		return m_WNutils;
	}

	private void initWordnet() {
		// Initialize the WordNet interface.
		String cwd = System.getProperty("user.dir");
		String wordnetdir = cwd + "/db/WordNetdb-3.0";
		System.setProperty("wordnet.database.dir", wordnetdir);
		// Instantiate 
		try {
			wordNet = WordNetDatabase.getFileInstance();
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
	}

	public void initMatchingHash()
	{
		m_hashRepeatedStr.clear();
	}

	public double areSynonyms(String source, String target, SynsetType type) {

		Synset[] sourceSynsets = wordNet.getSynsets(source, type);
		Synset[] targetSynsets = wordNet.getSynsets(target, type);
		double allsynsetScore = 0;

		allsynsetScore = compareTwoSynsetArray( sourceSynsets, targetSynsets);
		return allsynsetScore;

		//return false;
	}

	public boolean bAreSynonyms(String source, String target, SynsetType type) {

		Synset[] sourceSynsets = wordNet.getSynsets(source, type);
		Synset[] targetSynsets = wordNet.getSynsets(target, type);

		for (int i = 0; i < sourceSynsets.length; i++) {
			for (int j = 0; j < targetSynsets.length; j++) {
				if(sourceSynsets[i] == targetSynsets[j]){
					//System.out.println(source + " " + target + " synonyms!!");
					//isSynonym.put(key, true);
					return true;
				}
			}
		}
		//isSynonym.put(key, false);
		return false;
		//return bCompareTwoSynsetArray( sourceSynsets, targetSynsets, type);
	}

	public boolean bAreAntonyms(String source, String target, SynsetType type) {

		Synset[] sourceSynsets = wordNet.getSynsets(source, type);
		Synset[] targetSynsets = wordNet.getSynsets(target, type);

		for (int i = 0; i < sourceSynsets.length; i++) {
			for (int j = 0; j < targetSynsets.length; j++) {
				WordSense[] targetAntonymsSense = targetSynsets[j].getAntonyms(target);
				for (int k = 0; k < targetAntonymsSense.length; k++){
					Synset tarAntSynset = targetAntonymsSense[k].getSynset();
					if(sourceSynsets[i] == tarAntSynset){
						return true;
					}
				}
			}
		}

		return false;
	}

	public double areinstanceHypernyms(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		VerbSynset[] hypernymVerbSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );
		int allsynsetScore = 0;

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < targetSynsets.length; i++) {
				hypernymNounSynset = ((NounSynset) targetSynsets[i]).getInstanceHypernyms();
				allsynsetScore += compareTwoSynsetArray( srcSynsets, hypernymNounSynset);
			}
		}
		else if(type == SynsetType.VERB){
			for (int i = 0; i < targetSynsets.length; i++) {
				hypernymVerbSynset = ((VerbSynset)targetSynsets[i]).getHypernyms();
				allsynsetScore += compareTwoSynsetArray(srcSynsets, hypernymVerbSynset );
			}
		}

		//avoid divide by zero
		if(srcSynsets.length <= 0)
			return allsynsetScore;

		return allsynsetScore / srcSynsets.length;
		//return false;
	}

	public boolean bAreinstanceHypernyms(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		VerbSynset[] hypernymVerbSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < srcSynsets.length; i++) {
				hypernymNounSynset = ((NounSynset) srcSynsets[i]).getInstanceHypernyms();
				if( bCompareTwoSynsetArray( hypernymNounSynset, targetSynsets ) )
					return true;

				//get the hypernym synset of the hypernymsynset
				Queue<Synset> queue = new LinkedList<Synset>();
				queue.addAll(Arrays.asList(hypernymNounSynset));
				while(!queue.isEmpty()){
					hypernymNounSynset = ((NounSynset) queue.remove()).getHypernyms();

					if( bCompareTwoSynsetArray( hypernymNounSynset, targetSynsets) )
						return true;

					queue.addAll(Arrays.asList(hypernymNounSynset));
				}
			}
		}
		return false;
	}

	public boolean bAreHypernyms(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		VerbSynset[] hypernymVerbSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < targetSynsets.length; i++) {
				hypernymNounSynset = ((NounSynset) targetSynsets[i]).getHypernyms();
				if( bCompareTwoSynsetArray( srcSynsets, hypernymNounSynset) )
					return true;
			}
		}
		else if(type == SynsetType.VERB){
			for (int i = 0; i < targetSynsets.length; i++) {
				hypernymVerbSynset = ((VerbSynset)targetSynsets[i]).getHypernyms();
				if( bCompareTwoSynsetArray(srcSynsets, hypernymVerbSynset ))
					return true;
			}
		}

		return false;
	}

	public boolean bAreAncestorHypernyms(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		VerbSynset[] hypernymVerbSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		Queue<Synset> queue = new LinkedList<Synset>();
		queue.addAll(Arrays.asList(targetSynsets));
		int nOldCount = 0;
		int nNewCount = 0;
		
		//bfs traversal
		if(type == SynsetType.NOUN ){
			while(!queue.isEmpty()){
				hypernymNounSynset = ((NounSynset) queue.remove()).getHypernyms();

				if( bCompareTwoSynsetArray( srcSynsets, hypernymNounSynset) )
					return true;

				queue.addAll(Arrays.asList(hypernymNounSynset));
			}
		}
		else if(type == SynsetType.VERB){
			
			// Do not add all synsets: the verb synsets do not have a common parent 'entity'
			// these might have inter-related and might yield to deadlock
			HashSet<Synset> verbset = new HashSet<Synset>();
						
			while(!queue.isEmpty()){
				hypernymVerbSynset = ((VerbSynset) queue.remove()).getHypernyms();

				if( bCompareTwoSynsetArray( srcSynsets, hypernymVerbSynset) )
					return true;
				
				nOldCount = verbset.size();
				verbset.addAll(Arrays.asList(hypernymVerbSynset));
				nNewCount = verbset.size();
				
				if( nOldCount != nNewCount)
					queue.addAll(Arrays.asList(hypernymVerbSynset));
			}
		}
		
		return false;
	}

	public double areinstanceHyponyms(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );
		int allsynsetScore = 0;

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < targetSynsets.length; i++) {
				hypernymNounSynset = ((NounSynset) srcSynsets[i]).getInstanceHyponyms();
				allsynsetScore += compareTwoSynsetArray( srcSynsets, hypernymNounSynset  );
			}
		}

		//avoid divide by zero
		if(srcSynsets.length <= 0)
			return allsynsetScore;

		return allsynsetScore / srcSynsets.length;
		//return false;
	}

	public boolean bAreinstanceHyponyms(String source, String target, SynsetType type) {

		NounSynset[] hyponymNounSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < targetSynsets.length; i++) {
				hyponymNounSynset = ((NounSynset) targetSynsets[i]).getInstanceHyponyms();
				if( bCompareTwoSynsetArray( hyponymNounSynset, targetSynsets) )
					return true;

				//get the hypernym synset of the hypernymsynset
				Queue<Synset> queue = new LinkedList<Synset>();
				queue.addAll(Arrays.asList(hyponymNounSynset));
				while(!queue.isEmpty()){
					hyponymNounSynset = ((NounSynset) queue.remove()).getHypernyms();

					if( bCompareTwoSynsetArray( hyponymNounSynset, targetSynsets) )
						return true;

					queue.addAll(Arrays.asList(hyponymNounSynset));
				}
			}
		}

		return false;
	}

	public boolean bAreHyponyms(String source, String target, SynsetType type) {

		NounSynset[] hyponymNounSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < srcSynsets.length; i++) {
				hyponymNounSynset = ((NounSynset) srcSynsets[i]).getHyponyms();
				if( bCompareTwoSynsetArray( hyponymNounSynset,targetSynsets) )
					return true;
			}
		}

		return false;
	}

	public boolean bAreAncestorHyponyms(String source, String target, SynsetType type) {

		NounSynset[] hyponymNounSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		Queue<Synset> queue = new LinkedList<Synset>();
		queue.addAll(Arrays.asList(targetSynsets));

		//bfs traversal
		while(!queue.isEmpty()){
			hyponymNounSynset = ((NounSynset) queue.remove()).getHyponyms();

			if( bCompareTwoSynsetArray( srcSynsets, hyponymNounSynset) )
				return true;

			queue.addAll(Arrays.asList(hyponymNounSynset));
		}

		return false;
	}

	public double arePertainyms(String source, String target){
		int allsynsetScore = 0;

		Synset[] sourceSynsets = wordNet.getSynsets(target, SynsetType.NOUN);

		for (int i = 0; i < sourceSynsets.length; i++) {
			AdjectiveSynset[] adjSynset = ((NounSynset)sourceSynsets[i]).getAttributes();
			for(int j = 0; j < adjSynset.length; j++){
				WordSense[] relatedSensewords = adjSynset[j].getPertainyms(target);
				for(int k = 0; k < relatedSensewords.length; k++)
					allsynsetScore += compareTwoSynsets( sourceSynsets[i], relatedSensewords[k].getSynset());
			}
		}

		//avoid divide by zero
		if(sourceSynsets.length <= 0)
			return allsynsetScore;

		return allsynsetScore / sourceSynsets.length;
	}

	public boolean bArePertainyms(String source, String target, SynsetType type){
		Synset[] sourceSynsets = wordNet.getSynsets(source, type);

		if( type == SynsetType.ADVERB){ //adverb pertainyms gives adjective, so need to get target adj synset
			Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.ADJECTIVE);
			for (int i = 0; i < sourceSynsets.length; i++) {
				WordSense[] relatedSensewords = ((AdverbSynset)sourceSynsets[i]).getPertainyms(source);
				for(int j = 0; j < relatedSensewords.length; j++){
					Synset senseSynset = relatedSensewords[j].getSynset();
					for(int k = 0; k < targetSynsets.length; k++)
						if( bCompareTwoSynsets( senseSynset, targetSynsets[k]))
							return true;
				}
			}	
		}
		else if( type == SynsetType.ADJECTIVE){//adjective pertainyms gives noun, so need to get target noun synset
			Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.NOUN);
			for (int i = 0; i < sourceSynsets.length; i++) {
				WordSense[] relatedSensewords = ((AdjectiveSynset)sourceSynsets[i]).getPertainyms(source);
				for(int j = 0; j < relatedSensewords.length; j++){
					Synset senseSynset = relatedSensewords[j].getSynset();
					for(int k = 0; k < targetSynsets.length; k++)
						if( bCompareTwoSynsets( senseSynset, targetSynsets[k]))
							return true;
				}
			}	
		}

		return false;
	}

	public double areTopicMembers(String source, String target){

		Synset[] sourceSynsets = wordNet.getSynsets(source, SynsetType.NOUN );
		Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.NOUN);
		int allsynsetScore = 0;

		for (int i = 0; i < sourceSynsets.length; i++) {
			Synset[] topicMembersSynset = ((NounSynset)sourceSynsets[i]).getTopicMembers();
			allsynsetScore += compareTwoSynsetArray( topicMembersSynset, targetSynsets);
		}

		//avoid divide by zero
		if(sourceSynsets.length <= 0)
			return allsynsetScore;

		return allsynsetScore / sourceSynsets.length;
		//	return false;
	}

	public boolean bAreTopicMembers(String source, String target){

		Synset[] sourceSynsets = wordNet.getSynsets(source, SynsetType.NOUN );
		Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.NOUN);

		for (int i = 0; i < sourceSynsets.length; i++) {
			Synset[] topicMembersSynset = ((NounSynset)sourceSynsets[i]).getTopicMembers();
			if( bCompareTwoSynsetArray( topicMembersSynset, targetSynsets) )
				return true;
		}

		return false;
	}

	public double areOfTopics(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		Synset[] sourceSynsets = wordNet.getSynsets(source, type);
		Synset[] targetSynsets = wordNet.getSynsets(target, type);
		int allsynsetScore = 0;

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((NounSynset)sourceSynsets[i]).getTopics();
				allsynsetScore += compareTwoSynsetArray( hypernymNounSynset, targetSynsets );
			}
		}
		else if(type == SynsetType.VERB){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((VerbSynset)sourceSynsets[i]).getTopics();
				allsynsetScore += compareTwoSynsetArray( hypernymNounSynset, targetSynsets );
			}
		}
		else if(type == SynsetType.ADJECTIVE){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((AdjectiveSynset)sourceSynsets[i]).getTopics();
				allsynsetScore += compareTwoSynsetArray( hypernymNounSynset, targetSynsets );
			}
		}
		else if(type == SynsetType.ADVERB){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((AdverbSynset)sourceSynsets[i]).getTopics();
				allsynsetScore += compareTwoSynsetArray( hypernymNounSynset, targetSynsets );
			}
		}

		//return false;
		//avoid divide by zero
		if(sourceSynsets.length <= 0)
			return allsynsetScore;

		return allsynsetScore / sourceSynsets.length ;
	}	

	public boolean bAreOfTopics(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		Synset[] sourceSynsets = wordNet.getSynsets(source, type);
		Synset[] targetSynsets = wordNet.getSynsets(target, type);

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((NounSynset)sourceSynsets[i]).getTopics();
				if( bCompareTwoSynsetArray( hypernymNounSynset, targetSynsets ))
					return true;
			}
		}
		else if(type == SynsetType.VERB){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((VerbSynset)sourceSynsets[i]).getTopics();
				if(bCompareTwoSynsetArray( hypernymNounSynset, targetSynsets ))
					return true;
			}
		}
		else if(type == SynsetType.ADJECTIVE){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((AdjectiveSynset)sourceSynsets[i]).getTopics();
				if( bCompareTwoSynsetArray( hypernymNounSynset, targetSynsets ))
					return true;
			}
		}
		else if(type == SynsetType.ADVERB){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((AdverbSynset)sourceSynsets[i]).getTopics();
				if( bCompareTwoSynsetArray( hypernymNounSynset, targetSynsets ))
					return true;
			}
		}

		return false;
	}

	boolean bAreTroponyms(String source, String target){

		VerbSynset[] troponymVerbSynset;
		Synset[] sourceSynsets = wordNet.getSynsets(source, SynsetType.VERB);
		Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.VERB);

		//selectively choose the synset depending on the type
		for (int i = 0; i < sourceSynsets.length; i++) {
			troponymVerbSynset = ((VerbSynset)sourceSynsets[i]).getTroponyms();
			if( bCompareTwoSynsetArray( troponymVerbSynset, targetSynsets ))
				return true;
		}	

		return false;
	}

	boolean bAreSimilarAdj(String source, String target){
		
		AdjectiveSynset[] similarSynset;
		Synset[] sourceSynsets = wordNet.getSynsets(source, SynsetType.ADJECTIVE);
		Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.ADJECTIVE);

		//selectively choose the synset depending on the type
		for (int i = 0; i < sourceSynsets.length; i++) {
			similarSynset = ((AdjectiveSynset)sourceSynsets[i]).getSimilar();
			if( bCompareTwoSynsetArray( similarSynset, targetSynsets ))
				return true;
		}
		
		return false;
	}
	
	boolean bAreAdjofSameSatelliteHead(String source, String target){
	
		AdjectiveSynset srcHeadSynset;
		AdjectiveSynset tarHeadSynset;
		Synset[] sourceSynsets = wordNet.getSynsets(source, SynsetType.ADJECTIVE_SATELLITE);
		Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.ADJECTIVE_SATELLITE);

		//selectively choose the synset depending on the type
		for (int i = 0; i < sourceSynsets.length; i++) {
			srcHeadSynset = ((AdjectiveSatelliteReferenceSynset)sourceSynsets[i]).getHeadSynset();
			for (int j = 0; j < targetSynsets.length; j++){
				tarHeadSynset = ((AdjectiveSatelliteReferenceSynset)targetSynsets[j]).getHeadSynset();
				if( bCompareTwoSynsets( srcHeadSynset, tarHeadSynset ))
					return true;
			}
		}
		
		return false;
	}
	
	boolean bAreEntailments(String source, String target){

		VerbSynset[] troponymVerbSynset;
		Synset[] sourceSynsets = wordNet.getSynsets(source, SynsetType.VERB);
		Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.VERB);

		//selectively choose the synset depending on the type
		for (int i = 0; i < sourceSynsets.length; i++) {
			troponymVerbSynset = ((VerbSynset)sourceSynsets[i]).getEntailments();
			if( bCompareTwoSynsetArray( troponymVerbSynset, targetSynsets ))
				return true;
		}	

		return false;
	}
	
	boolean bAreofSameVerbGroup(String source, String target){
		
		Synset[] sourceSynsets = wordNet.getSynsets(source, SynsetType.VERB );
		Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.VERB);

		for (int i = 0; i < sourceSynsets.length; i++) {
			VerbSynset[] verbMembersSynset = ((VerbSynset)sourceSynsets[i]).getVerbGroup();
			if( bCompareTwoSynsetArray( verbMembersSynset, targetSynsets) )
				return true;
		}

		return false;
	}

	double compareTwoSynsetArray(Synset[] srcSynset, Synset[] tarSynset){

		double allsynsetScore = 0;
		double totalSynsetsLength = srcSynset.length + tarSynset.length;

		for (int j = 0; j < srcSynset.length; j++) {
			for (int k = 0; k < tarSynset.length; k++) {
				allsynsetScore += compareTwoSynsets( srcSynset[j], tarSynset[k] );
			}
		}

		//avoid divide by zero
		if(totalSynsetsLength <= 0)
			return allsynsetScore;

		return allsynsetScore / totalSynsetsLength;
	}

	boolean bCompareTwoSynsetArray(Synset[] srcSynset, Synset[] tarSynset){

		for (int j = 0; j < srcSynset.length; j++) {
			for (int k = 0; k < tarSynset.length; k++) {
				if( bCompareTwoSynsets( srcSynset[j], tarSynset[k] ) )
					return true;
			}
		}

		return false;
	}

	double compareTwoSynsets(Synset srcSynset, Synset tarSynset){

		String[] srcArray = srcSynset.getWordForms();
		String[] tarArray = tarSynset.getWordForms();
		int totalScore = 0;
		int totalSynsetLength = 0;

		for(int i = 0; i < srcArray.length; i++){
			for(int j = 0; j < tarArray.length; j++){
				totalSynsetLength += srcArray[i].length() + tarArray[j].length();
			}
		}

		for(int i = 0; i < srcArray.length; i++){
			for(int j = 0; j < tarArray.length; j++){
				int score = srcArray[i].toLowerCase().compareTo(tarArray[j]);

				//normalize score
				score = score * (srcArray[i].length() + tarArray[j].length()) / totalSynsetLength ;	
				totalScore += score;
			}
		}

		return totalScore;
	}

	boolean bCompareTwoSynsets(Synset srcSynset, Synset tarSynset){
		return srcSynset == tarSynset ? true : false;
	}


	//	boolean bcompareTwoSynsets(Synset srcSynset, Synset tarSynset, SynsetType type){
	//
	//		String[] srcArray = srcSynset.getWordForms();
	//		String[] tarArray = tarSynset.getWordForms();
	//		
	//		//split the definitions and find if there are any common words
	//		List<String> POStags = new ArrayList<String>();
	//		if(type == SynsetType.NOUN)
	//			POStags.add("NN");
	//		
	//		int i = 0;
	//		String srcString = new String();
	//		String tarString = new String();
	//		for(String str: srcArray)
	//			srcString += str + " ";
	//		
	//		i = 0;
	//		for(String str: tarArray)
	//			tarString += str + " ";
	//			
	//		List<String> srcWFList = m_Parser.getPOSWordsofSentence(srcString, POStags);
	//		String[] srcWFArray = srcWFList.toString().split(" ");
	//		
	//		List<String> tarWFList = m_Parser.getPOSWordsofSentence(tarString, POStags);
	//		String[] tarWFArray = tarWFList.toString().split(" ");
	//
	//		//compare word forms
	//		if(bCompareTwoStringArrays(srcWFArray, tarWFArray))
	//			return true;		
	//
	//		String srcDef = srcSynset.getDefinition();
	//		List<String> srcDefList = m_Parser.getPOSWordsofSentence(srcDef, POStags);
	//		String[] srcDefArray = srcDefList.toString().split(" ");
	//		       
	//		String tarDef = tarSynset.getDefinition();
	//		List<String> tarDefList = m_Parser.getPOSWordsofSentence(tarDef, POStags);
	//		String[] tarDefArray = tarDefList.toString().split(" ");
	//		
	//		//compare word definations
	//		if(bCompareTwoStringArrays(srcDefArray, tarDefArray))
	//			return true;
	//
	//		return false; // else
	//	}
	//
	//	boolean bCompareTwoStringArrays(String[] srcArray, String[] tarArray){
	//		for(int i = 0; i < srcArray.length; i++){
	//			for(int j = 0; j < tarArray.length; j++){
	//				
	//				//stem the strings before comparing
	//				String srcStem = srcArray[i].toLowerCase().replaceAll("[\\[,\\]]", " ").trim();
	//				srcStem = m_PStemmer.stem(srcStem);
	//
	//				String tarStem = tarArray[j].toLowerCase().replaceAll("[\\[,\\]]", " ").trim();
	//				tarStem = m_PStemmer.stem(tarStem);
	//
	//				if( /*srcStem.contains(tarStem) || tarStem.contains(srcStem) ||*/srcStem.equalsIgnoreCase(tarStem))
	//				{
	//					if(m_hashRepeatedStr.containsKey(srcStem) || m_hashRepeatedStr.containsValue(tarStem))
	//						return false; // no need to add score repeatedly
	//					
	//					m_hashRepeatedStr.put(srcStem, tarStem);
	//					return true;
	//				}
	//					
	//			}
	//		}
	//		return false;
	//	}

	public String[] getLemma(String word, SynsetType type){
		if(m_morph == null) return null;
		return	m_morph.getBaseFormCandidates(word, type);
	}

	public boolean isaAttribute(String word/*eg: height: table attribute*/, String/*high, low  of the question*/ attribute){
		Synset[] syn =  wordNet.getSynsets(word);

		//check whether the words has synsets in wordnet
		if(syn == null || syn.length <= 0)
			return false;

		for(int i = 0; i < syn.length; i++){		

			if(SynsetType.NOUN == syn[i].getType()){
				AdjectiveSynset[] attrsyn = ((NounSynset)syn[i]).getAttributes();

				//check whether the word has attributes in the wordnet
				if(attrsyn == null || attrsyn.length <= 0)
					continue;

				//check whether present
				for(int j = 0; j < attrsyn.length; j++){
					List<String> attrList = Arrays.asList(attrsyn[j].getWordForms());
					if( attrList.contains(attribute) ){
						return true;
					}

					//or whether synonyms or antonyms : attribute of a noun is adj
					for(String attr: attrList){
						if( bAreSynonyms(attr, attribute, SynsetType.ADJECTIVE) ||
								bAreAntonyms(attr, attribute, SynsetType.ADJECTIVE) ||
								bArePertainyms(attr, attribute, SynsetType.ADJECTIVE)){
							return true;
						}
					}
				}

				//or word's instance = attribute or topic members
				for(String attr: syn[i].getWordForms()){
					if( bAreinstanceHypernyms(attr, attr, SynsetType.NOUN) ||
							bAreTopicMembers(attribute, attr) )
						return true;
				}
			}
			else if(SynsetType.ADJECTIVE == syn[i].getType()){
				NounSynset[] attrsyn = ((AdjectiveSynset)syn[i]).getAttributes();

				//check whether the word has attributes in the wordnet
				if(attrsyn == null || attrsyn.length <= 0)
					continue;

				//check whether present
				for(int j = 0; j < attrsyn.length; j++){
					List<String> attrList = Arrays.asList(attrsyn[j].getWordForms());
					if( attrList.contains(attribute) ){
						return true;
					}

					//or whether synonyms or antonyms or word's instance = attribute
					//or topic members : attribute of a noun is ad
					for(String attr: attrList){
						if( bAreSynonyms(attr, attribute, SynsetType.NOUN) ||
								bAreAntonyms(attr, attribute, SynsetType.NOUN) ||
								bArePertainyms(attr, attribute, SynsetType.NOUN) ){
							return true;
						}
					}	
				}

				//or word's instance = attribute or topic members
				for(String attr: syn[i].getWordForms()){
					if( bAreinstanceHypernyms(attr, attr, SynsetType.ADJECTIVE) ||
							bAreTopicMembers(attribute, attr) )
						return true;
				}
			}
		}

		return false;
	}
	
	public String getCountryFromInstance(String instance){ //like french -> france, canadian -> canada

		Synset[] countrySynsets = wordNet.getSynsets(instance, SynsetType.ADJECTIVE);
		Synset senseSynset = null;

		for (int i = 0; i < countrySynsets.length; i++) {
			WordSense[] relatedSensewords = ((AdjectiveSynset)countrySynsets[i]).getPertainyms(instance);

			if( null != relatedSensewords){
				senseSynset = relatedSensewords[0].getSynset();
				return senseSynset.getWordForms()[0];
			}
		}
		
		return null;
	}

	public static void main(String[] args){
		WordNetUtils utils = new WordNetUtils();

		boolean bStatus = utils.isaAttribute("height", "high");
		System.out.println(bStatus);

		/*boolean bIsRelated = utils.bAreAntonyms("good", "bad", SynsetType.NOUN);
		System.out.println("0 )" + bIsRelated);

		bIsRelated = utils.bAreSynonyms("good", "bad", SynsetType.NOUN);
		System.out.println("1 ) " + bIsRelated);

		bIsRelated = utils.bAreSynonyms("beautiful", "ugly", SynsetType.ADJECTIVE);
		System.out.println("2 )" + bIsRelated);

		bIsRelated = utils.bAreHypernyms("vehicle", "car", SynsetType.NOUN);
		System.out.println("3 )" +bIsRelated);

		bIsRelated = utils.bAreHyponyms("vehicle", "car", SynsetType.NOUN);
		System.out.println("4 )" +bIsRelated);

		bIsRelated = utils.bAreAncestorHypernyms("vehicle", "lorry", SynsetType.NOUN);
		bIsRelated = utils.bAreAncestorHypernyms("feline", "lion", SynsetType.NOUN);
		System.out.println("5 )" +bIsRelated);

		bIsRelated = utils.bAreAncestorHyponyms("lorry", "vehicle", SynsetType.NOUN);
		System.out.println("6 )" +bIsRelated);

		bIsRelated = utils.bAreinstanceHypernyms("Mississippi", "river", SynsetType.NOUN);
		System.out.println("7 )" +bIsRelated);*/

		//boolean bIsRelated = utils.bAreinstanceHypernyms("France", "country", SynsetType.NOUN); 
		//System.out.println("7 )" +bIsRelated);

		//String[] lemmas = utils.wordNet.getBaseFormCandidates("beautiful", SynsetType.NOUN);

		//System.out.print(lemmas[0]);

/*		boolean bIsRelated = utils.bAreAncestorHypernyms("frisson", "brain", SynsetType.NOUN);
		System.out.println("5 )" +bIsRelated);

		bIsRelated = utils.bAreTroponyms("verbalize", "shout");
		System.out.println("6 )" +bIsRelated);

		bIsRelated = utils.bAreEntailments("snore", "sleep");
		System.out.println("7 )" +bIsRelated);

		bIsRelated = utils.bArePertainyms("academic", "academia", SynsetType.ADJECTIVE);
		System.out.println("8 )" +bIsRelated);
		
		bIsRelated = utils.bAreofSameVerbGroup("talk", "write");
		System.out.println("10 )" +bIsRelated);
		
		bIsRelated = utils.bAreinstanceHypernyms("Tunisia", "country", SynsetType.NOUN);
		System.out.println("11 )" +bIsRelated);
		
		bIsRelated = utils.bAreAncestorHypernyms("verbalize", "shout", SynsetType.VERB);
		System.out.println("12 )" +bIsRelated);*/
		
		boolean bIsRelated = utils.bAreAncestorHypernyms("organization", "country", SynsetType.VERB);
		System.out.println("12 )" + bIsRelated);
		
		bIsRelated = utils.bAreAncestorHypernyms("country", "organization", SynsetType.VERB);
		System.out.println("13 )" + bIsRelated);
	}

}