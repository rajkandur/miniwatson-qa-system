package category;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import common.CommonData;
import common.CommonData.QUESTIONTYPE;
import common.CommonData.SQLTABLES;

import qawparser.OpenNLPWrapParser;

import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.impl.file.Morphology;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.HirstStOnge;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.impl.LeacockChodorow;
import edu.cmu.lti.ws4j.impl.Lesk;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.impl.Path;
import edu.cmu.lti.ws4j.impl.Resnik;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;

/**
 * @author kandur
 *
 */
public class CategoryIdentification {

	SQLTABLES m_eSqlTab;	
	HashMap<SQLTABLES, List<String>> m_TableIdentifierData;
	OpenNLPWrapParser m_parser;
	WordNetUtils m_WordnetUtils;
	private String m_data;
	private double m_olympicsScore;
	private double m_oscarScore;
	private double m_geoGraphyScore;
	PorterStemmer m_PortStemmer;

	public CategoryIdentification() {
		m_TableIdentifierData = new HashMap<CommonData.SQLTABLES, List<String>>();
		m_parser = OpenNLPWrapParser.getInstance();
		m_WordnetUtils = WordNetUtils.getinstance();
		m_PortStemmer = new PorterStemmer();
		initIdentifierData();

		m_data = new String();
		m_data += "Sentence, OlympicScore, OscarScore, GeoScore, ClassLabel\n";
		//m_data += "Sentence, ClassLabel\n";
	}	

	/**
	 * @author kandur
	 *Initializes all the table identifiers(attributes) to its respective table
	 */
	void initIdentifierData(){

		if(m_TableIdentifierData == null)
			return;		

		List<String> olympicsData = new ArrayList<String>();
		//Given semantics of OLYMPICS
		olympicsData.add("olympics");
		olympicsData.add("Results");
		olympicsData.add("Winner");
		olympicsData.add("Medal");
		olympicsData.add("gold");
		olympicsData.add("silver");
		olympicsData.add("Athletes");
		olympicsData.add("Nationality");
		olympicsData.add("Gender");
		olympicsData.add("Competition");
		olympicsData.add("race");
		olympicsData.add("winter");
		olympicsData.add("sports");
		m_TableIdentifierData.put(SQLTABLES.OLYMPICS, olympicsData);

		List<String> oscarData = new ArrayList<String>();
		//Given semantics of IMDB Oscar movie
		oscarData.add("Oscar");
		oscarData.add("Movie");
		oscarData.add("rating");
		//oscarData.add("year");
		oscarData.add("genre");
		//oscarData.add("rank");
		//oscarData.add("runtime");
		oscarData.add("person");
		oscarData.add("date"); //dob
		//oscarData.add("birth"); //dob
		oscarData.add("actor");
		oscarData.add("director");
		oscarData.add("star");
		m_TableIdentifierData.put(SQLTABLES.OSCAR, oscarData);

		List<String> geographyData = new ArrayList<String>();
		//Given semantics of IMDB Oscar movie
		geographyData.add("Geography");
		geographyData.add("city");
		geographyData.add("capital");
		geographyData.add("border");
		geographyData.add("sea");
		geographyData.add("oceans");
		geographyData.add("continent");
		geographyData.add("country");
		geographyData.add("population");
		geographyData.add("mountain");
		m_TableIdentifierData.put(SQLTABLES.GEOGRAPHY, geographyData);
	}

	/**
	 * @author kandur
	 * Extract nouns and proper nouns from the chunks and try to find the whether relation terms exist as part of the table.
	 * Extract verbs from the chunks and try to find the whether relation terms exist as part of the table.
	 */
	public void validateRules(){
		for(String sent: m_parser.getListofquestions()){
			identifyQuestionType(sent);
		}
	}

	public SQLTABLES identifyQuestionType(String question){
		//sent = "Did Razzoli win the men slalom in 2010?";
		//sent = m_parser.getListofquestions().get(2);

		List<String> Nountags = new ArrayList<String>();
		Nountags.add("NN"); //all the noun forms, this acts as root form of all type of nouns

		//extract the NN forms and generate the score from the table data
		List<String> listOfNouns = m_parser.getPOSWordsofSentence(question, Nountags);

		List<String> Verbtags = new ArrayList<String>();
		Verbtags.add("VB"); //all forms of VB are checked.of all type of nouns

		//extract the VB forms and generate the score from the table data
		List<String> listOfVerbs = m_parser.getPOSWordsofSentence(question, Verbtags);

		//calculate the table score for all the terms for the given sentence
		m_WordnetUtils.initMatchingHash();
		m_olympicsScore = calculateScore(SQLTABLES.OLYMPICS, listOfNouns, SynsetType.NOUN);
		m_olympicsScore += calculateScore(SQLTABLES.OLYMPICS, listOfVerbs, SynsetType.VERB);

		m_WordnetUtils.initMatchingHash();
		m_oscarScore = calculateScore(SQLTABLES.OSCAR, listOfNouns, SynsetType.NOUN);
		m_oscarScore += calculateScore(SQLTABLES.OSCAR, listOfVerbs, SynsetType.VERB);

		m_WordnetUtils.initMatchingHash();
		m_geoGraphyScore = calculateScore(SQLTABLES.GEOGRAPHY, listOfNouns, SynsetType.NOUN);
		m_geoGraphyScore += calculateScore(SQLTABLES.GEOGRAPHY, listOfVerbs, SynsetType.VERB);		

		//from the score label the table
		m_data += question + "," + m_olympicsScore + "," + m_oscarScore + "," + m_geoGraphyScore;
		//m_data += sent;

		String label;
		//if boolean comparison, higher score = higher similarity
		SQLTABLES eDBType;

		if( m_olympicsScore > m_oscarScore ){
			if(m_geoGraphyScore > m_olympicsScore){
				label = "GEOGRAPHY"; eDBType = SQLTABLES.GEOGRAPHY;}
			else if(m_geoGraphyScore < m_olympicsScore){
				label = "OLYMPICS"; eDBType = SQLTABLES.OLYMPICS;}
			else{
				label = "OLYMPICS / GEOGRAPHY"; eDBType = SQLTABLES.OLYMPICS; }//can be geography as well}
		}
		else if(m_olympicsScore < m_oscarScore ){
			if(m_geoGraphyScore > m_oscarScore){
				label = "GEOGRAPHY"; eDBType = SQLTABLES.GEOGRAPHY;}
			else if(m_geoGraphyScore < m_oscarScore){
				label = "OSCAR"; eDBType = SQLTABLES.OSCAR;}
			else{
				label = "OSCAR / GEOGRAPHY"; eDBType = SQLTABLES.OSCAR; }//can be geography as well
		}
		else{
			if(m_geoGraphyScore > m_olympicsScore ) {//m_oscarScore: since both will be same here
				label = "GEOGRAPHY"; eDBType = SQLTABLES.GEOGRAPHY;}
			else
				label = "OSCAR / OLYMPICS"; eDBType = SQLTABLES.OSCAR;
		}

		m_data += "," + label + "\n";
		
		return eDBType;
	}


	/*	double calculateScore(SQLTABLES eSqlTableName, List<String> listOfTargetData, SynsetType type){

		if(!m_TableIdentifierData.containsKey(eSqlTableName) || null == m_WordnetUtils)
			return 0;

		List<String> listOfSrcTableData = m_TableIdentifierData.get(eSqlTableName);

		double score = 0.0;
		for(String src: listOfSrcTableData){	
			for(String tar: listOfTargetData){
				//String source = m_PortStemmer.stem(src);
				//String target = m_PortStemmer.stem(tar);

				score += CommonData.getSemanticSimilarity(src, tar);
			}
		}

		return score / (listOfSrcTableData.size() + listOfTargetData.size());
	}*/



	double calculateScore(SQLTABLES eSqlTableName, List<String> listOfTargetData, SynsetType type){

		if(!m_TableIdentifierData.containsKey(eSqlTableName) || null == m_WordnetUtils)
			return 0;

		List<String> listOfSrcTableData = m_TableIdentifierData.get(eSqlTableName);
		//		listOfSrcTableData.clear();
		//		listOfSrcTableData.add("good");
		//		listOfTargetData.clear();
		//		listOfTargetData.add("beautiful");
		//		type =  SynsetType.ADJECTIVE;

		double score = 0;
		for(String source: listOfSrcTableData){	
			for(String target: listOfTargetData){
				//are synonyms
				if(m_WordnetUtils.bAreSynonyms(source, target, type))
					score+=10;
				//are instancehypernyms
				if(m_WordnetUtils.bAreinstanceHypernyms(source, target, type) ||
						m_WordnetUtils.bAreinstanceHypernyms(target, source, type) )
					score++;
				//are instancehyponyms
				if(m_WordnetUtils.bAreinstanceHyponyms(source, target, type) ||
						m_WordnetUtils.bAreinstanceHyponyms(target, source, type ))
					score++;
				//are hypernyms
				if(m_WordnetUtils.bAreHypernyms(source, target, type))
					score++;
				//are hyponyms
				if(m_WordnetUtils.bAreHyponyms(source, target, type))
					score++;
				//are pertainyms
				if(m_WordnetUtils.bArePertainyms(source, target, type))
					score++;
				//are topicmembers
				if(m_WordnetUtils.bAreTopicMembers(source, target))
					score++;
				//are topics
				if(m_WordnetUtils.bAreOfTopics(source, target, type))
					score++;
				//}
			}
		}

		return score;
	}

	public void writecsvfile(){
		try {
			OutputStream os = new FileOutputStream("Classify.csv");
			os.write(m_data.getBytes());
			os.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args){
		//double sim = CommonData.getSemanticSimilarity("height", "high");
		//System.out.println(sim);

		CategoryIdentification cat = new CategoryIdentification();
		SQLTABLES type = cat.identifyQuestionType("Is Rome the capital of Italy?");
		System.out.println(type);		
		
		//OpenNLPWrapParser parser = OpenNLPWrapParser.getInstance();
		//parser.loadquestions();
		//cat.validateRules();
		//cat.writecsvfile();
	}
}
