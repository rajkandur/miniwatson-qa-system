package category;

import java.io.ByteArrayInputStream;

/**
 * 
 * @author kandur
 * 
 * A upper level class for underlying Standard Stemmer Algorithm
 */
public class PorterStemmer {
	private Stemmer m_Stemmer;
	private String m_StemmedStr;
	private String m_OriginalStr;

	public PorterStemmer() {
		m_Stemmer = new Stemmer();
	}

	String stem(String str){
		
		m_OriginalStr =  str;
		char[] w = new char[m_OriginalStr.length()];
		ByteArrayInputStream in = new ByteArrayInputStream(str.getBytes());

		while(true){ 
			int ch = in.read();
			if (Character.isLetter((char) ch))
			{
				int j = 0;
				while(true){  
					ch = Character.toLowerCase((char) ch);
					w[j] = (char) ch;
					if (j < m_OriginalStr.length()-1) j++;
					ch = in.read();
					if (!Character.isLetter((char) ch))
					{
						/* to test add(char ch) */
						for (int c = 0; c < j; c++) m_Stemmer.add(w[c]);

						/* or, to test add(char[] w, int j) */
						/* m_Stemmer.add(w, j); */

						m_Stemmer.stem();
						
						/* and now, to test toString() : */
						m_StemmedStr = m_Stemmer.toString();

						/* to test getResultBuffer(), getResultLength() : */
						/* u = new String(m_Stemmer.getResultBuffer(), 0, m_Stemmer.getResultLength()); */

						//System.out.println(m_StemmedStr);
						break;
					}
				}
			}
			if (ch < 0) break;
			//System.out.print((char)ch);
		}	
		return m_StemmedStr;
	}
}

