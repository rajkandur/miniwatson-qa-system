package tts;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

public class Voices {
	
	public static void main(String[] args) {
		VoiceManager voiceManager;
        
        voiceManager = VoiceManager.getInstance();
        // Get all available voices
        Voice[] voices = voiceManager.getVoices();
        
        for (int i = 0; i < voices.length; i++) {
             System.out.println(voices[i].getName());
        }
	}

}
