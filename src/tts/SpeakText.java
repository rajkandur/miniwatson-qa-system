package tts;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

public class SpeakText {
	
	String textToSpeak;
	String voiceName;
	
	public SpeakText(String textToSpeak, String voice) {
		this.textToSpeak = textToSpeak;
		this.voiceName = voice;
		speakString();
	}
	
	public void speakString(){
		VoiceManager voiceManager = null;
        Voice voice = null;
        
		voiceManager = VoiceManager.getInstance();
        voice = voiceManager.getVoice(voiceName);
        voice.setStyle("business");
        
        voice.allocate();
        
        voice.speak(textToSpeak);

        voice.deallocate();
	}

	/*public static void main(String[] args) {
		new SpeakText("Hello this is Mark", "kevin16");
	}*/
	
}
