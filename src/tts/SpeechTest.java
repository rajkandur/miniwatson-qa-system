package tts;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

public class SpeechTest {

	private String speaktext;

	public void doSpeak(String speak, String voice) {
		speaktext = speak;
		try {
			VoiceManager voiceManager = VoiceManager.getInstance();
			Voice voices = voiceManager.getVoice(voice);
			Voice sp = null;
			if (voices != null)
				sp = voices;
			else
				System.out.println("No Voice Available");
			// ==================================================
			sp.allocate();
			sp.speak(speaktext);
			sp.deallocate();
			// ==================================================
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*public static void main(String[]args)
	   {
		//System.setProperty("mbrola.base", Morbola.class.getName());
		SpeechTest obj = new SpeechTest();
	    obj.doSpeak("Is Rome the capital of Italy?", "Kelvin16");
	   }*/
	
	public static void main(String[] args) {
		// Voice Name
        String voiceName = "kevin16";
        VoiceManager voiceManager = null;
        Voice voice = null;

        voiceManager = VoiceManager.getInstance();
        voice = voiceManager.getVoice(voiceName);

       /* voice.setPitch((float) 4.00);
        voice.setPitchShift((float) .005);
        voice.setPitchRange((float) 0.01);*/
        // "business", "casual", "robotic", "breathy"
        voice.setStyle("business");
       
        //allocate the resources for the voice
        voice.allocate();
        
        voice.speak("Is Rome the capital of Italy?");

        voice.deallocate();
	}

}
